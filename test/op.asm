# Programa de teste para MIF32ASM e MIF32SIM
# Realiza operações + - & | ^ e ~| entre um número lido e o número 5.

# Instruções usadas
#
# [ ] addi      [-] divu      [-] mult      [ ] sra
# [ ] addiu     [ ] jal       [-] multu     [ ] srav
# [X] add       [X] j         [X] nor       [ ] srl
# [ ] addu      [X] jr        [X] ori       [ ] srlv
# [ ] andi      [ ] lb        [X] or        [X] sub
# [X] and       [ ] lbu       [ ] sb        [ ] subu
# [ ] beq       [X] lui       [ ] sll       [X] sw
# [ ] bltz      [X] lw        [ ] sllv      [X] syscall
# [X] bne       [-] mfhi      [ ] slti      [ ] xori
# [-] div       [-] mflo      [ ] slt       [X] xor


      .data 
a:          .word   0
b:          .word   5
res:        .word   0
str_entraa: .asciiz "entra a: "  
str_valorb: .asciiz "valor b: "
str_sum:    .asciiz "\n  a + b  = "
str_dif:    .asciiz "\n  a - b  = "
str_and:    .asciiz "\n  a & b  = "
str_or:     .asciiz "\n  a | b  = "
str_xor:    .asciiz "\n  a ^ b  = "
str_nor:    .asciiz "\n~(a | b) = "

      .text
main: li   $v0, 4             # Serviço Print String
      la   $a0, str_entraa    # Carrega endereço da string do prompt
      syscall                 # Imprime string
      li   $v0, 5             # Serviço Read Integer
      syscall                 # Lê um inteiro
      la   $t0, a             # Carrega endereço de a
      sw   $v0, 0($t0)        # Armazena o valor lido em a
      li   $v0, 4             # Serviço Print String
      la   $a0, str_valorb    # Carrega endereço da string str_valorb
      syscall                 # Imprime string
      la   $t0, b             # Carrega endereço de b
      lw   $a0, 0($t0)        # Carrega valor de b
      li   $v0, 1             # Serviço Print Integer
      syscall                 # Imprime inteiro
      la   $t0, sum           # Carrega endereço do código de soma
      la   $t1, a             # Carrega endereço de a
      la   $t2, b             # Carrega endereço de b
      lw   $t3, 0($t1)        # Carrega valor de a
      lw   $t4, 0($t2)        # Carrega valor de b
loop: jr   $t0                # Pula para código da próxima operação
  
sum:  la   $a0, str_sum       # Código para soma
      add  $t5, $t3, $t4
      la   $t0, dif
      j    print
dif:  la   $a0, str_dif       # Código para subtração
      sub  $t5, $t3, $t4
      la   $t0, and
      j    print
and:  la   $a0, str_and       # Código para and
      and  $t5, $t3, $t4
      la   $t0, or
      j    print
or:   la   $a0, str_or        # Código para or
      or   $t5, $t3, $t4
      la   $t0, xor
      j    print
xor:  la   $a0, str_xor       # Código para xor
      xor  $t5, $t3, $t4
      la   $t0, nor
      j    print
nor:  la   $a0, str_nor       # Código para nor
      nor  $t5, $t3, $t4
      move $t0, $0

print: li  $v0, 4             # Serviço Print String
      syscall                 # Imprime string
      la   $t1, res           # Carrega endereço do resultado
      sw   $t5, 0($t1)        # Armazena resultado
      li   $v0, 1             # Serviço Print Integer
      move $a0, $t5           # Carrega inteiro a imprimir 
      syscall                 # Imprime inteiro
      bne  $t0, $0, loop      # Vai para código da próxima operação
      li   $v0, 10            # Serviço para encerrar o programa
      syscall                 # Fim

