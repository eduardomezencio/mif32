# Adaptado de http://courses.missouristate.edu/KenVollmar/MARS/Fibonacci.asm
#
# Computa os doze primeiros números de Fibonacci,
# coloca-os em um vetor e então os imprime

# Instruções usadas
#
# [X] addi      [-] divu      [-] mult      [ ] sra
# [ ] addiu     [ ] jal       [-] multu     [ ] srav
# [X] add       [ ] j         [ ] nor       [ ] srl
# [ ] addu      [ ] jr        [X] ori       [ ] srlv
# [ ] andi      [ ] lb        [ ] or        [ ] sub
# [ ] and       [ ] lbu       [ ] sb        [ ] subu
# [ ] beq       [X] lui       [ ] sll       [X] sw
# [ ] bltz      [X] lw        [ ] sllv      [X] syscall
# [X] bne       [-] mfhi      [ ] slti      [ ] xori
# [-] div       [-] mflo      [X] slt       [ ] xor


      .data

fibs: .word   0 : 12        # "vetor" de 12 words para conter valores de fib
size: .word   12            # Tamanho do "vetor"
      .space  32
space:.asciiz  " "          # Espaço para escrever entre números
head: .asciiz  "Os numeros de Fibonacci sao:\n"   

      .text

      la   $t0, fibs        # Carrega endereço do vetor
      la   $t5, size        # Carrega endereço do tamanho
      lw   $t5, 0($t5)      # Carrega tamanho
      li   $t2, 1           # 1 é o primeiro e o segundo número de Fibonacci
      sw   $t2, 0($t0)      # F[0] = 1
      sw   $t2, 4($t0)      # F[1] = F[0] = 1
      addi $t1, $t5, -2     # Contador para loop, vai executar (tamanho-2) vezes
loop: lw   $t3, 0($t0)      # Carrega valor do vetor F[n] 
      lw   $t4, 4($t0)      # Carrega valor do vetor F[n+1]
      add  $t2, $t3, $t4    # $t2 = F[n] + F[n+1]
      sw   $t2, 8($t0)      # Armazena F[n+2] = F[n] + F[n+1] no vetor
      addi $t0, $t0, 4      # Incrementa endereço do vetor
      addi $t1, $t1, -1     # Decrementa contador de loop
      bgt  $t1, $0, loop    # Repete se não tiver chegado ao fim
      la   $a0, fibs        # Primeiro argumento para impressão (vetor)
      add  $a1, $zero, $t5  # Segundo argumento para impressão (tamanho)
print:add  $t0, $zero, $a0  # Endereço inicial do vetor
      add  $t1, $zero, $a1  # Inicializar contador de loop para tamanho do vetor
      la   $a0, head        # Carrega o endereço da string de apresentação
      li   $v0, 4           # Especifica serviço Print String
      syscall               # Imprime apresentação
out:  lw   $a0, 0($t0)      # Carrega número de Fibonacci para syscall
      li   $v0, 1           # Especifica serviço Print Integer
      syscall               # Imprime número de Fibonacci
      la   $a0, space       # Carrega endereço do espaço para syscall
      li   $v0, 4           # Especifica serviço Print String
      syscall               # Imprime espaço
      addi $t0, $t0, 4      # Incrementa endereço
      addi $t1, $t1, -1     # Decrementa contador do loop
      bgt  $t1, $0, out     # Repete se não tiver chegado ao fim
      li   $v0, 10          # Serviço de syscall para encerrar o programa
      syscall               # Fim

