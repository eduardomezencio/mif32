/**
 * @file   io.c
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Implementação do módulo de entrada e saída.
 *
 * Este arquivo implementa o módulo de entrada e saída.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include "io.h"
#include "label_list.h"

FILE *i, *o;
int   current_line = 0;
int   echo_on      = 0; /*< Se o programa deve imprimir as entradas que recebe
                            de volta na saída. Útil quando a entrada não vem do
                            usuário, mas de um pipe, por exemplo. */

/**
 * @brief Inicia entrada e saída.
 *
 * Inicia entrada e saída usando os arquivos dados nos argumentos ou os arquivos
 * de entrada e saída padrão. Para usar stdin/stdout, passar a string "-" como
 * nome do arquivo de entrada ou saída, respectivamente.
 *
 * @param in_file   Nome do arquivo de entrada.
 * @param out_file  Nome do arquivo de saída.
 *
 * @return          Retorna 1 caso haja erro ou 0 caso contrário.
 */
int io_init(const char *in_file, const char *out_file)
{
    const char msg[] = "Erro: Não foi possível abrir %s para %s.\n",
                 r[] = "leitura", w[] = "escrita";

    if(in_file != NULL)
    {
        if(strcmp(in_file, "-") == 0)
            i = stdin;
        else
            i = fopen(in_file, "r");
        if(i == NULL)
        {
            fprintf(stderr, msg, in_file, r);
            return 1;
        }
        current_line = 0;
    }

    if(out_file != NULL)
    {
        if(strcmp(out_file, "-") == 0)
            o = stdout;
        else
            o = fopen(out_file, "w");

        if(o == NULL)
        {
            fprintf(stderr, msg, out_file, w);
            if(i != NULL) fclose(i);
            return 1;
        }
    }

    return 0;
}

/**
 * @brief Finaliza entrada e saída.
 *
 * Finaliza entrada e saída, liberando os arquivos que foram abertos.
 */
void io_close()
{
    if(i != NULL && i != stdin ) fclose(i);
    if(o != NULL && o != stdout) fclose(o);
    current_line = 0;
}

/**
 * @brief Retorna número da linha sendo lida.
 *
 * @return  Retorna número da linha atual no arquivo de entrada.
 */
int io_line_number()
{
    return current_line;
}

/**
 * @brief Habilita ou desabilita eco.
 *
 * Habilita ou desabilita eco. Quando eco está habilitado, a leitura de dados
 * no arquivo de entrada é seguida da escrita desses dados no arquivo de saída.
 *
 * @param echo  Valor 1 habilita eco, 0 desabilita.
 */
void io_set_echo(int echo)
{
    echo_on = (echo != 0);
}

/**
 * @brief Lê uma linha.
 *
 * Lê uma linha do arquivo de entrada para o buffer passado como argumento. Este
 * buffer deve ter ao menos IO_LINE_BUFFER_SIZE de tamanho. Caso eco esteja
 * habilitado, essa função ira imprimir de volta no arquivo de saída a linha que
 * foi lida.
 *
 * @param dest  Buffer para armazenar a linha lida.
 *
 * @return      Retorna ponteiro para o buffer ou NULL se não foi possível ler.
 */
char *io_read_line(char *dest)
{
    char *l = fgets(dest, IO_LINE_BUFFER_SIZE, i);
    current_line++;
    if(l && echo_on)
    {
        fprintf(o, "%s", l);
        if(l[strlen(l) - 1] != '\n')
            fprintf(o, "\n");
    }
    return l;
}

/**
 * @brief Lê uma linha não vazia.
 *
 * Lê linhas do arquivo de entrada até encontrar uma linha não vazia e coloca
 * essa linha não vazia no buffer passado como argumento. Este buffer deve ter
 * ao menos IO_LINE_BUFFER_SIZE de tamanho. Caso eco esteja habilitado, essa
 * função ira imprimir de volta no arquivo de saída a linha que foi lida.
 *
 * @param dest  Buffer para armazenar a linha lida.
 *
 * @return      Retorna ponteiro para o buffer ou NULL se não foi possível ler.
 */
char *io_read_line_ne(char *dest)
{
    while(1)
    {
        char *l = fgets(dest, IO_LINE_BUFFER_SIZE, i);
        if(l == NULL) return NULL;

        current_line++;
        char c;
        int read = sscanf(l, " %c", &c);
        if(read > 0)
        {
            if(echo_on)
            {
                fprintf(o, "%s", l);
                if(l[strlen(l) - 1] != '\n')
                    fprintf(o, "\n");
            }
            return l;
        }
    }
}

/**
 * @brief Escreve uma word.
 *
 * Escreve a word dada em hexadecimal no arquivo de saída, seguida de uma quebra
 * de linha.
 *
 * @param word  A word a ser escrita.
 */
void io_write_word(uint32_t word)
{
    fprintf(o, "%08"PRIX32"\n", word);
}

/**
 * @brief Escreve uma linha.
 *
 * Escreve a string dada no arquivo de saída, seguida de uma quebra de linha.
 *
 * @param str  A string a ser escrita.
 */
void io_write_line(const char *str)
{
    fprintf(o, "%s\n", str);
}

/**
 * @brief Escreve uma lista de labels formatados.
 *
 * Escreve a lista de labels dada no formato que é lido pelo simulador. Cada
 * label é precedido pelo caracter '=' para indicar ao simulador que esta linha
 * deve ser ignorada e é seguido pelo endereço do label.
 *
 * @param labels  A lista de labels.
 */
void io_write_labels(const list_t *labels)
{
    struct label_list_entry *e;
    int maxlen = 1;
    list_for_each_entry(labels, e, node)
    {
        int len = strlen(e->name);
        if(len > maxlen) maxlen = len;
    }

    list_for_each_entry(labels, e, node)
    {
        char format[12], line[64];
        sprintf(format, "= %%%ds %%08X", maxlen);
        sprintf(line, format, e->name, e->address);
        fprintf(o, "%s\n", line);
    }
}

/**
 * @brief Escreve uma string.
 *
 * Escreve a string dada no arquivo de saída.
 *
 * @param str  A string a ser escrita.
 */
void io_write_string (const char *str)
{
    fprintf(o, "%s", str);
}

