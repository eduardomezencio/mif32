/**
 * @file   instr_dict.c
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Implementação do dicionário de instruções.
 *
 * Implementa o dicionário de instruções.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "instr_dict.h"
#include "io.h"

list_t instr_dict;

#define ARG_TABLE_SIZE 8
const struct
{
    instr_arg_t t;
    char        s[INSTR_MAX_ARG_SIZE+1];
}
arg_table[ARG_TABLE_SIZE] =
{
    {arg_none        , "-"    },
    {arg_rs          , "$s"   },
    {arg_rt          , "$t"   },
    {arg_rd          , "$d"   },
    {arg_immed       , "C"    },
    {arg_immed_offset, "C($s)"},
    {arg_label       , "L"    },
    {arg_shamt       , "A"    }
};

/**
 * @brief Inicia o dicionário de instruções.
 *
 * O dicionário é inciciado e preenchido com os dados lidos dos arquivos 'instr'
 * e 'pseudo'.
 */
int instr_dict_init()
{
    FILE     *f;
    char      name[INSTR_MAX_NAME_SIZE+1];
    char      format;
    char      args[(INSTR_MAX_ARG_SIZE+1)*INSTR_MAX_ARGS];
    char      arg_types[INSTR_MAX_ARGS+1];
    char     *arg_token;
    uint16_t  opcode;
    uint8_t   instr_c;

    list_init(&instr_dict);
    f = fopen("instr", "r");
    if(f == NULL)
    {
        print_error("Não foi possível abrir o dicionário de instruções.");
        return 1;
    }

    while(fscanf(f, "%s %c %"SCNx16" %s", name, &format, &opcode, args) != EOF)
    {
        dict_entry_t *e = malloc(sizeof(dict_entry_t));
        strcpy(e->name, name);
        e->type = 'i';

        struct instr_info *i = &(e->info.i);
        i->format = format;
        i->opcode = opcode;

        int arg = 0;
        for(arg_token = strtok(args,","); arg_token != NULL;
            arg_token = strtok(NULL,","), arg++)
        {
            for(int j = 0; j < ARG_TABLE_SIZE; j++)
            {
                if(strcmp(arg_token, arg_table[j].s)) continue;
                i->args[arg] = arg_table[j].t;
                break;
            }
        }
        while(arg <= INSTR_MAX_ARGS)
            i->args[arg++] = arg_none;

        list_append(&instr_dict, &(e->node));
    }

    fclose(f);
    f = fopen("pseudo", "r");
    if(f == NULL)
    {
        print_error("Não foi possível abrir o dicionário de pseudo-instruções.");
        return 1;
    }

    while(fscanf(f, " %s %s %"SCNu8" \n", name, arg_types, &instr_c) != EOF)
    {
        dict_entry_t *e = malloc(sizeof(dict_entry_t));
        strcpy(e->name, name);
        e->type = 'p';

        struct pseudo_info *i = &(e->info.p);
        strcpy(i->arg_types, arg_types);
        i->arg_c   = strlen(arg_types);
        i->instr_c = instr_c;

        int line_count;
        for(line_count = 0; line_count < instr_c; line_count++)
        {
            char line[64];
            fgets(line, 64, f);
            sscanf(line, " %s %[^\n]", i->instr_names[line_count], i->instr_args[line_count]);
        }
        for(; line_count < PSEUDO_MAX_INSTR; line_count++)
            i->instr_names[line_count][0] = i->instr_args[line_count][0] = '\0';

        list_append(&instr_dict, &(e->node));
    }

    fclose(f);
    return 0;
}

/**
 * @brief Libera o dicionário de instruções.
 *
 * Libera a memória utilizada pela lista do dicionário de instruções.
 */
void instr_dict_free()
{
    dict_entry_t *pos, *n;
    list_for_each_entry_safe(&instr_dict, pos, n, node)
        free(pos);
}

/**
 * @brief Busca uma instrução no dicionário.
 *
 * Busca a instrução com o nome @c name e retorna a entrada correspondente.
 *
 * @param  name O nome da instrução a ser buscada.
 * @return      Retorna a entrada do dicionário correspondente ou NULL, no caso
 *              de não encontrar a instrução.
 */
const dict_entry_t *instr_dict_search(char *name)
{
    dict_entry_t *pos;
    list_for_each_entry(&instr_dict, pos, node)
    {
        if(strcmp(pos->name, name) == 0)
            return pos;
    }
    return NULL;
}

/**
 * @brief Retorna o número de instruções correspondente a uma entrada do dicionário.
 *
 * Retorna o número de instruções que uma entrada do dicionário gera. Útil para
 * pseudo-instruções. No caso de intruções normais, o valor é sempre 1.
 *
 * @param  entry A entrada do dicionário.
 * @return       Retorna o número de instruções gerada por @c entry.
 */
int get_instr_c(const dict_entry_t *entry)
{
    if(entry->type == 'i')
        return 1;
    else if(entry->type == 'p')
        return entry->info.p.instr_c;
    else
        return 0;
}

