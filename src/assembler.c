/**
 * @file   assembler.c
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Implementação do assembler.
 *
 * Este arquivo contém a implementação das funções do assembler.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#include <assert.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "assembler.h"
#include "instr_asm.h"
#include "io.h"
#include "label_list.h"
#include "string_helper.h"

#define DIR_MAX_NAME_SIZE  7
#define DIR_TABLE_SIZE     6
const struct
{
    enum assembler_directives  d;
    char                       s[DIR_MAX_NAME_SIZE+1];
}
dir_table[DIR_TABLE_SIZE] =
{
    { DIR_DATA   , ".data"   },
    { DIR_TEXT   , ".text"   },
    { DIR_BYTE   , ".byte"   },
    { DIR_WORD   , ".word"   },
    { DIR_ASCIIZ , ".asciiz" },
    { DIR_SPACE  , ".space"  }
};

struct assembler_info info;

static int  handle_directive   (char *dir  , char *args, bool gen_code, list_t *out);
static int  handle_instruction (char *instr, char *args, bool gen_code, list_t *out);
static int  handle_pseudo      (list_t *lines, list_t *out);

static int  section_change     (enum assembler_section section, bool gen_code);
static int  directive_byte     (char *args, bool gen_code, list_t *out);
static int  directive_word     (char *args, bool gen_code, list_t *out);
static int  directive_asciiz   (char *args, bool gen_code, list_t *out);
static int  directive_space    (char *args, bool gen_code, list_t *out);

static void word_list_append   (list_t *l, uint32_t word);
static void word_list_free     (list_t *l);

/**
 * @brief Inicia o assembler.
 *
 * Inicia o assembler, atribuindo valores iniciais às informações de estado.
 *
 * @param in_file   Nome do arquivo de entrada.
 * @param out_file  Nome do arquivo de saída
 *
 * @return          Retorna 0 normalmente, ou 1 no caso de erro.
 */
int assembler_init(char *in_file, char *out_file)
{
    info.in_file  = malloc(strlen( in_file) + 1);
    info.out_file = malloc(strlen(out_file) + 1);
    strcpy(info.in_file , in_file );
    strcpy(info.out_file, out_file);

    info.stack_size = STACK_DEFAULT_SIZE;

    int error = instr_dict_init();
    if(error) return error;

    label_list_init();

    return 0;
}

/**
 * @brief Fecha o assembler.
 *
 * Fecha o assembler. Essa função libera a memória alocada pelo assembler, pelo
 * dicionário e pela lista de labels.
 */
void assembler_close()
{
    free(info.in_file);
    free(info.out_file);

    instr_dict_free();
    label_list_free();
}

/**
 * @brief Primeiro passo do assembler.
 *
 * O primeiro passo do assembler. Este passo serve para preencher a lista de
 * labels com os endereços corretos e calcular a memória necessária para cada
 * seção.
 *
 * @return  Retorna 0 normalmente, ou 1 no caso de erro.
 */
int assembler_first_pass()
{
    char line[IO_LINE_BUFFER_SIZE];

    info.section = SECTION_NONE;
    info.address = 0x00000000;
    io_init(info.in_file, NULL);

    while(io_read_line(line))
    {
        char *instr = NULL, *label = NULL, *args = NULL;
        instr = sep_comment(line, NULL);
        instr = sep_label(instr, &label);
        if(label)
        {
            label = trim_space(label);
            int error = label_list_append(label, info.address, info.section);
            if(error)
            {
                io_close();
                return 1;
            }
        }

        instr = sep_args(instr, &args);
        if(instr == NULL) continue;
        if(args) args = trim_space(args);

        int (*handle_func)(char*, char*, bool, list_t*) =
            (instr[0] == '.') ? handle_directive : handle_instruction;
        int error = handle_func(instr, args, false, NULL);
        if(error)
        {
            io_close();
            return 1;
        }
    }

    section_change(SECTION_FINISH, false);

    io_close();
    return 0;
}

/**
 * @brief Segundo passo do assembler.
 *
 * O segundo passo do assembler. Este passo é responsável pela geração de
 * código.
 *
 * @return  Retorna 0 normalmente, ou 1 no caso de erro.
 */
int assembler_second_pass()
{
    char line[IO_LINE_BUFFER_SIZE];

    info.section = SECTION_NONE;
    info.address = 0x00000000;
    io_init(info.in_file, info.out_file);

    while(io_read_line(line))
    {
        char *instr = NULL, *label = NULL, *args = NULL;
        instr = sep_comment(line, NULL);
        instr = sep_label(instr, NULL);
        instr = sep_args(instr, &args);
        if(instr == NULL) continue;
        if(args) args = trim_space(args);

        list_t out;
        list_init(&out);

        int (*handle_func)(char*, char*, bool, list_t*) =
            (instr[0] == '.') ? handle_directive : handle_instruction;
        int error = handle_func(instr, args, true, &out);

        if(!error)
        {
            struct word_list_entry *word;
            list_for_each_entry(&out, word, node)
                io_write_word(word->word);
        }

        word_list_free(&out);
        if(error)
        {
            io_close();
            return 1;
        }
    }

    section_change(SECTION_FINISH, true);

    io_close();
    return 0;
}

/**
 * @brief Tratamento de diretivas.
 *
 * Identifica uma diretiva e a trata, transferindo o trabalho para a função
 * adequada.
 *
 * @param dir       String com o nome da diretiva.
 * @param args      String com os argumentos da diretiva.
 * @param gen_code  Valor booleano que indica se código deve ser gerado.
 * @param out       Se gen_code for @c true, a lista de words que receberá
 *                  o código gerado.
 *
 * @return          Retorna 0 normalmente, ou 1 no caso de erro.
 */
int handle_directive(char *dir, char *args, bool gen_code, list_t *out)
{
    int d;
    for(d = 0; d < DIR_TABLE_SIZE && strcmp(dir, dir_table[d].s) != 0; d++);

    switch(d)
    {
    case DIR_DATA:
        return section_change   (SECTION_DATA, gen_code);
    case DIR_TEXT:
        return section_change   (SECTION_TEXT, gen_code);
    case DIR_BYTE:
        return directive_byte   (args, gen_code, out);
    case DIR_WORD:
        return directive_word   (args, gen_code, out);
    case DIR_ASCIIZ:
        return directive_asciiz (args, gen_code, out);
    case DIR_SPACE:
        return directive_space  (args, gen_code, out);
    default:
        print_error_ln_f("Diretiva %s desconhecida.", dir);
        return 1;
    }

    return 0;
}

/**
 * @brief Tratamento de instruções.
 *
 * Identifica uma instrução e a trata. Se for gerar código, delega esta função
 * a instruction_encode, no caso de uma instrução normal. No caso de uma
 * pseudo-instrução, chama pseudo_expand para gerar uma lista de instruções
 * normais e as passa para handle_pseudo.
 *
 * @param instr     String com o nome da instrução.
 * @param args      String com os argumentos da instrução.
 * @param gen_code  Valor booleano que indica se código deve ser gerado.
 * @param out       Se gen_code for @c true, a lista de words que receberá
 *                  o código gerado.
 *
 * @return  Retorna 0 normalmente, ou 1 no caso de erro.
 */
int handle_instruction(char *instr, char *args, bool gen_code, list_t *out)
{
    if(info.section != SECTION_TEXT)
    {
        print_error_ln("Instruções devem estar na seção text.");
        return 1;
    }

    const dict_entry_t *e = instr_dict_search(instr);
    if(e == NULL)
    {
        print_error_ln_f("Instrução %s não existe.", instr);
        return 1;
    }

    if(!gen_code)
    {
        info.address += get_instr_c(e) * 4;
        return 0;
    }

    if(e->type == 'p')
    {
        list_t lines;
        int error;
        error = pseudo_expand(e, args, &lines);
        if(error) return 1;
        error = handle_pseudo(&lines, out);
        string_list_free(&lines);
        return error;
    }
    else if(e->type == 'i')
    {
        instruction_t instr;
        uint32_t bin;
        int error;

        error = instruction_encode(e, args, info.address, &instr);
        if(error) return 1;

        info.address += 4;
        word_list_append(out, instruction_to_bin(&instr));
        return 0;
    }

    assert(0);
}

/**
 * @brief Tratamento de pseudo-instruções.
 *
 * Trata uma lista de strings gerada pela função pseudo_expand, a partir de
 * uma pseudo instrução,
 *
 * @param lines    Ponteiro para lista de strings com as linhas de código.
 * @param out      Ponteiro para a lista de words que receberá o código gerado.
 *
 * @return  Retorna 0 normalmente, ou 1 no caso de erro.
 */
int handle_pseudo(list_t *lines, list_t *out)
{
    string_list_entry_t *line;
    list_for_each_entry(lines, line, node)
    {
        char *instr = NULL, *args = NULL;
        instr = sep_args(line->str, &args);
        if(args) args = trim_space(args);

        int error = handle_instruction(instr, args, true, out);
        if(error) return 1;
    }

    return 0;
}

/**
 * @brief Tratamento de mudança de seção.
 *
 * Trata mudanças de seção do assembler.
 *
 * @param section   A seção para qual o assembler deve mudar.
 * @param gen_code  Valor booleano que indica se código deve ser gerado. No
 *                  caso dessa função, o código é escrito direto no arquivo.
 *
 * @return  Retorna 0 normalmente, ou 1 no caso de erro.
 */
int section_change(enum assembler_section section, bool gen_code)
{
    switch(section)
    {
    case SECTION_DATA:

        if(info.section != SECTION_NONE)
        {
            print_error_ln("Seção data deve ser a primeira seção.");
            return 1;
        }

        if(gen_code)
        {
            char line[32];
            uint32_t size = info.data_size + info.stack_size;
            sprintf(line, ".data %"PRIu32, size);
            io_write_line(line);
            io_write_labels(get_data_labels());
        }

        info.section = SECTION_DATA;
        info.address = SECTION_OFFSET_DATA;
        return 0;

    case SECTION_TEXT:

        if(info.section != SECTION_DATA)
        {
            print_error_ln("Seção text deve vir após seção data.");
            return 1;
        }

        if(gen_code)
        {
            char line[32];
            sprintf(line, ".text %d", info.text_size);
            io_write_line(".enddata");
            io_write_line(line);
            io_write_labels(get_text_labels());
        }
        else
        {
            info.data_size = (info.address - SECTION_OFFSET_DATA) / 4;
            info.data_size += 32 - (info.data_size % 4);
        }

        info.section = SECTION_TEXT;
        info.address = SECTION_OFFSET_TEXT;
        return 0;

    case SECTION_FINISH:

        if(info.section != SECTION_TEXT)
        {
            print_error_ln("Fim de arquivo sem seção text.");
            return 1;
        }

        if(gen_code)
            io_write_line(".endtext");
        else
        {
            info.text_size = (info.address - SECTION_OFFSET_TEXT) / 4;
            info.text_size += 32 - (info.text_size % 4);
        }

        info.section = SECTION_FINISH;
        return 0;

    default:
        assert(0);
    }
}

/**
 * @brief Tratamento da diretiva .byte .
 *
 * Trata a diretiva .byte . Essa diretiva gera um byte ou uma lista de bytes
 * a partir dos valores passados como argumento. Se o armazenamento não
 * terminar alinhado em uma word, preenche com 0 o restante da word.
 *
 * @param args      String com os argumentos passados à diretiva.
 * @param gen_code  Valor booleano que indica se código deve ser gerado. No
 *                  caso dessa função, o código é escrito direto no arquivo.
 * @param out       Ponteiro para a lista de words que receberá o código gerado.
 *
 * @return          Retorna 0 normalmente, ou 1 no caso de erro.
 */

int directive_byte(char *args, bool gen_code, list_t *out)
{
    int      count = 0, pos = 0;
    uint8_t  byte  = 0x00;
    uint32_t word  = 0x00000000;

    if(info.section != SECTION_DATA)
    {
        print_error_ln("Diretiva .byte deve estar na seção data.");
        return 1;
    }

    for(char *c = strtok(args, ","); c != NULL; c = strtok(NULL, ","))
    {
        if(sscanf(c, " %"SCNu8, &byte) <= 0)
        {
            print_error_ln_f("Byte '%s' inválido.", c);
            word_list_free(out);
            return 1;
        }

        pos = count++ % 4;
        if(!gen_code) continue;

        word |= byte << ((3 - pos) * 8);
        if(pos == 3)
        {
            word_list_append(out, word);
            word = 0x00000000;
        }
    }

    if(count % 4)
    {
        if(gen_code) word_list_append(out, word);
        count += 4 - (count % 4);
    }

    info.address += count;

    return 0;
}

/**
 * @brief Tratamento da diretiva .word .
 *
 * Trata a diretiva .word . Essa diretiva gera um word ou uma lista de words
 * a partir dos valores passados como argumento.
 *
 * @param args      String com os argumentos passados à diretiva.
 * @param gen_code  Valor booleano que indica se código deve ser gerado. No
 *                  caso dessa função, o código é escrito direto no arquivo.
 * @param out       Ponteiro para a lista de words que receberá o código gerado.
 *
 * @return          Retorna 0 normalmente, ou 1 no caso de erro.
 */
int directive_word(char *args, bool gen_code, list_t *out)
{
    int      count = 0;
    uint32_t word  = 0x00000000, rep;

    if(info.section != SECTION_DATA)
    {
        print_error_ln("Diretiva .word deve estar na seção data.");
        return 1;
    }

    for(char *c = strtok(args, ","); c != NULL; c = strtok(NULL, ","))
    {
        int s = sscanf(c, " %"SCNu32" : %"SCNu32, &word, &rep);
        if(s <= 0)
        {
            print_error_ln_f("Word '%s' inválida.", c);
            word_list_free(out);
            return 1;
        }
        else if(s < 2) rep = 1;

        count += rep;
        if(gen_code) while(rep--) word_list_append(out, word);
    }

    info.address += count * 4;

    return 0;
}

/**
 * @brief Tratamento da diretiva .asciiz .
 *
 * Trata a diretiva .asciiz . Essa diretiva gera uma sequência de bytes
 * correspondente a uma string terminada em caracter nulo '\0'. Se a string não
 * terminar alinhada em uma word, preenche com 0 o restante da word.
 *
 * @param args      String com os argumentos passados à diretiva.
 * @param gen_code  Valor booleano que indica se código deve ser gerado. No
 *                  caso dessa função, o código é escrito direto no arquivo.
 * @param out       Ponteiro para a lista de words que receberá o código gerado.
 *
 * @return          Retorna 0 normalmente, ou 1 no caso de erro.
 */
int directive_asciiz(char *args, bool gen_code, list_t *out)
{

    int      count = 0, pos  = 0;
    uint32_t word  = 0x00000000;

    if(info.section != SECTION_DATA)
    {
        print_error_ln("Diretiva .asciiz deve estar na seção data.");
        return 1;
    }

    char *last = args + strlen(args) - 1;
    bool error = (args[0] != '"' || *last != '"');
    *last = '\0';

    if(!error) while(*(++args) != '\0')
    {
        if(*args == '"')
        {
            error = true;
            break;
        }

        char c = *args;
        if(*args == '\\')
        {
            switch(*(++args))
            {
            case 'a' : c = '\a'; break;
            case 'b' : c = '\b'; break;
            case 't' : c = '\t'; break;
            case 'n' : c = '\n'; break;
            case 'v' : c = '\v'; break;
            case 'f' : c = '\f'; break;
            case 'r' : c = '\r'; break;
            case '"' : c = '"' ; break;
            case '\\': c = '\\'; break;
            default:
                print_error_ln("Caracter de escape inválido.");
                return 1;
            }
        }

        pos = count++ % 4;
        if(!gen_code) continue;

        word |= (uint32_t)c << (pos * 8);
        if(pos == 3)
        {
            word_list_append(out, word);
            word = 0x00000000;
        }
    }

    if(error)
    {
        print_error_ln_f("String %s inválida.", args);
        return 1;
    }

    if(gen_code) word_list_append(out, word);
    count += 4 - (count % 4);

    info.address += count;

    return 0;
}

/**
 * @brief Tratamento da diretiva .space .
 *
 * Trata a diretiva .space . Essa diretiva gera uma sequência de bytes
 * vazios, com valor zero. Se o número de bytes não terminar alinhado em uma
 * word, preenche com 0 o restante da última word.
 *
 * @param args      String com os argumentos passados à diretiva.
 * @param gen_code  Valor booleano que indica se código deve ser gerado. No
 *                  caso dessa função, o código é escrito direto no arquivo.
 * @param out       Ponteiro para a lista de words que receberá o código gerado.
 *
 * @return          Retorna 0 normalmente, ou 1 no caso de erro.
 */
int directive_space(char *args, bool gen_code, list_t *out)
{
    uint32_t space;

    if(info.section == SECTION_NONE)
    {
        print_error_ln("Diretiva .space deve estar na seção data ou text.");
        return 1;
    }

    if(sscanf(args, "%"SCNu32, &space) <= 0)
    {
        print_error_ln_f("Valor %s inválido para diretiva .space.", args);
        return 1;
    }

    {
        int i;
        for(i = 0; i < space; i += 4)
            if(gen_code) word_list_append(out, 0x00000000);
        info.address += i;
    }

    return 0;
}

/**
 * @brief Insere word ao final de uma lista de words.
 *
 * Insere a word passada como argumento ao final de uma lista de words.
 *
 * @param l     Ponteiro para a lista de words.
 * @param word  A word a ser inserida.
 */
void word_list_append(list_t *l, uint32_t word)
{
    struct word_list_entry *w = malloc(sizeof(struct word_list_entry));
    w->word = word;
    list_append(l, &(w->node));
}

/**
 * @brief Libera uma lista de words
 *
 * Libera a memória alocada por uma lista de words.
 *
 * @param l  Ponteiro para a lista de words.
 */
void word_list_free(list_t *l)
{
    struct word_list_entry *pos, *n;
    list_for_each_entry_safe(l, pos, n, node)
        free(pos);
}
