/**
 * @file   label_list.c
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Implementação da lista de labels.
 *
 * Este arquivo define as funções da lista de labels.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "io.h"
#include "label_list.h"
#include "list.h"

list_t labels_data;
list_t labels_text;

/**
 * @brief Inicia as listas de labels.
 *
 * Inicia as listas de labels.
 */
void label_list_init()
{
    list_init(&labels_data);
    list_init(&labels_text);
}

/**
 * @brief Libera as listas de labels.
 *
 * Libera a memória alocada para as listas de labels.
 */
void label_list_free()
{
    struct label_list_entry *pos, *n;
    list_for_each_entry_safe(&labels_data, pos, n, node)
        free(pos);
    list_for_each_entry_safe(&labels_text, pos, n, node)
        free(pos);
}

/**
 * @brief Insere um label ao final da lista correspondente.
 *
 * Insere o label passado como argumento na lista correspondente, dependendo
 * da seção passada (section_data ou section_text)
 *
 * @param name    O nome do label.
 * @param address O endereço do label.
 * @param section A seção do código onde o label se encontra (section_data ou
 *                section_text)
 *
 * @return        Retorna 0 normalmente ou 1 se ocorrer um erro.
 */
int label_list_append(const char *name, uint32_t address,
                       enum assembler_section section)
{
    if(section == SECTION_NONE)
    {
        print_error_ln("Labels devem estar na seção data ou text.");
        return 1;
    }

    struct label_list_entry *l = malloc(sizeof(struct label_list_entry));
    strcpy(l->name, name);
    l->address = address;
    list_append(section == SECTION_DATA
        ? &labels_data
        : &labels_text, &(l->node));

    return 0;
}

/**
 * @brief Retorna o endereço de um label.
 *
 * Retorna o endereço do label passado como argumento, caso ele exista.
 *
 * @param name  O nome do label.
 *
 * @return      Retorna o endereço do label ou LABEL_NOT_FOUND se ele não
 *              estiver na lista.
 */
uint32_t get_label_address(const char *name)
{
    struct label_list_entry *pos;

    list_for_each_entry(&labels_data, pos, node)
    {
        if(strcmp(pos->name, name) == 0)
            return pos->address;
    }

    list_for_each_entry(&labels_text, pos, node)
    {
        if(strcmp(pos->name, name) == 0)
            return pos->address;
    }

    print_error_ln_f("Label %s não encontrado.", name);
    return LABEL_NOT_FOUND;
}

/**
 * @brief   Retorna o endereço da lista de labels da seção data.
 *
 * @return  Retorna o endereço da lista de labels da seção data.
 */
list_t *get_data_labels()
{
    return &labels_data;
}

/**
 * @brief   Retorna o endereço da lista de labels da seção text.
 *
 * @return  Retorna o endereço da lista de labels da seção text.
 */
list_t *get_text_labels()
{
    return &labels_text;
}

