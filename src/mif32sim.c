/**
 * @file   mif32sim.c
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Definição da função main para o simulador.
 *
 * Este arquivo apenas define a função main do simulador. Dentro de main, são
 * verificados os parâmetros da linha de comando e então o simulador é chamado
 * para executar o programa.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#include <stdio.h>
#include <string.h>
#include "simulator.h"
#include "io.h"

int main(int argc, char **argv)
{
    const char in_stdin[] = "-";
    const char *in = argc > 1 ? argv[1] : in_stdin;

    int error = simulator_init(in);
    if(error) return 1;

    int (*step[])() = {&simulator_fetch, &simulator_decode, &simulator_execute};
    int step_n = 0;

    int status = SIMULATOR_OK;
    while(status == SIMULATOR_OK)
    {
        status = step[step_n++]();
        step_n %= 3;
    }

    error = (status == SIMULATOR_ERROR);
    if(error)
        print_error("Erro no simulador. Abortando.");

    simulator_close();
    return error;
}
