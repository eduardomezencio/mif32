/**
 * @file   instruction.c
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Definição das funções para conversão de instruções.
 *
 * Definição das funções para conversão entre estruturas de instrução e formato
 * binário de instrução.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#include <assert.h>
#include "instruction.h"
#include "io.h"

/**
 * @brief Converte @c instruction_t em código de máquina.
 *
 * Recebe como entrada um @c struct @c instruction e retorna como saída uma
 * word de 32 bits correspondente à instrição na arquitetura MIF32
 *
 * @param src  A instrução a ser convertida.
 *
 * @return     A representação da instrução em uma word de 32 bits.
 */
uint32_t instruction_to_bin(const instruction_t *src)
{
    switch(src->format)
    {
    case 'R':
        return ((src->data.r.rs     << 21) |
                (src->data.r.rt     << 16) |
                (src->data.r.rd     << 11) |
                (src->data.r.shamt  <<  6) |
                 src->data.r.funct       ) ;
    case 'I':
        return ((src->data.i.opcode << 26) |
                (src->data.i.rs     << 21) |
                (src->data.i.rt     << 16) |
                 src->data.i.immediate   ) ;
    case 'J':
        return ((src->data.j.opcode << 26) |
                 src->data.j.address     ) ;
    }

    assert(0);
}

/**
 * @brief Converte código de máquina em @c instruction_t.
 *
 * Recebe como entrada uma instrução em 32 bits e converte em um @c struct
 * @c instruction com informações da instrução.
 *
 * @param src   A instrução a ser convertida.
 * @param dest  Ponteiro do @c struct @c instruction que receberá a instrução.
 *
 * @return      Retorna o argumento dest.
 */
instruction_t *bin_to_instruction(uint32_t src, instruction_t *dest)
{
    assert(dest != NULL);

    uint8_t opcode = (src & 0xFC000000) >> 26;
    char    format;

    if      (opcode == 0)                format = 'R';
    else if (opcode == 2 || opcode == 3) format = 'J';
    else                                 format = 'I';

    dest->format = format;

    switch(format)
    {
    case 'R':
        dest->data.r.rs        = (src & 0x03E00000) >> 21;
        dest->data.r.rt        = (src & 0x001F0000) >> 16;
        dest->data.r.rd        = (src & 0x0000F800) >> 11;
        dest->data.r.shamt     = (src & 0x000007C0) >>  6;
        dest->data.r.funct     = (src & 0x0000003F);
        break;
    case 'I':
        dest->data.i.opcode    = (src & 0xFC000000) >> 26;
        dest->data.i.rs        = (src & 0x03E00000) >> 21;
        dest->data.i.rt        = (src & 0x001F0000) >> 16;
        dest->data.i.immediate = (src & 0x0000FFFF);
        break;
    case 'J':
        dest->data.j.opcode    = (src & 0xFC000000) >> 26;
        dest->data.j.address   = (src & 0x03FFFFFF);
        break;
    }

    return dest;
}
