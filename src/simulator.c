/**
 * @file   simulator.c
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Implementação do simulador.
 *
 * Este arquivo define as funções do simulador.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#include <assert.h>
#include <ctype.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "execute.h"
#include "io.h"
#include "simulator.h"

struct simulator_data sim;

int read_header (char *line_buffer, enum simulator_section s);
int read_labels (char *line_buffer);
int read_data   (char *line_buffer, enum simulator_section s);
int read_footer (char *line_buffer, enum simulator_section s);

/**
 * @brief Inicia o simulador.
 *
 * Inicia o simulador, lendo o arquivo passado e carregando nas memórias de
 * dados e de instruções.
 *
 * @param filename  Nome do arquivo a ser lido.
 *
 * @return          Retorna 1 se houve erro ou 0 caso contrário.
 */
int simulator_init(const char *filename)
{
    sim.pc                 = 0x00000000;
    sim.ir                 = 0x00000000;
    sim.data_memory        = NULL;
    sim.instruction_memory = NULL;
    memset(sim.registers, 0x00, 128);

    if(io_init(filename, "-")) return 1;

    char line[IO_LINE_BUFFER_SIZE], *tok;

    int error = read_header(line, SECTION_DATA);
    if(!error) error = read_labels(line);
    if(!error) error = read_data  (line, SECTION_DATA);
    if(!error) error = read_footer(line, SECTION_DATA);

    if(!error) error = read_header(line, SECTION_TEXT);
    if(!error) error = read_labels(line);
    if(!error) error = read_data  (line, SECTION_TEXT);
    if(!error) error = read_footer(line, SECTION_TEXT);

    io_close();

    if(error)
    {
        simulator_close();
    }
    else
    {
        if(strcmp(filename, "-") == 0)
            io_set_echo(1);
            
        io_init("-", "-");
        //simulator_print_memory(SECTION_TEXT); // DEBUG
    }

    return error;
}

/**
 * @brief Fecha o simulador.
 *
 * Fecha os arquivos de entrada e saída e libera a memória alocada para o
 * simulador.
 */
void simulator_close()
{
    io_write_line("");
    io_close();

    free(sim.data_memory);
    free(sim.instruction_memory);
    sim.data_memory        = NULL;
    sim.instruction_memory = NULL;
}

/**
 * @brief Etapa fetch do simulador.
 *
 * Executa a etapa fetch do simulador, onde a instrução no endereço do PC é
 * lida para IR e PC é incrementado.
 *
 * @return  Retorna um valor simulator_result. SIMULATOR_ERROR no caso de
 *          erro ou SIMULATOR_OK caso contrário.
 */
int simulator_fetch()
{
    if(sim.pc < sim.instruction_memory_size)
    {
        //fprintf(stderr, " PC: %08"PRIX32, sim.pc);     // DEBUG
        sim.ir = sim.instruction_memory[sim.pc++];
        //fprintf(stderr, " IR: %08"PRIX32"\n", sim.ir); // DEBUG
        return SIMULATOR_OK;
    }

    print_error("Leitura de endereço fora da memória de instruções. Abortando.");
    return SIMULATOR_ERROR;
}

/**
 * @brief Etapa decode do simulador.
 *
 * Executa a etapa decode do simulador, onde a instrução no IR é decodificada.
 *
 * @return  Retorna um valor simulator_result. Sempre SIMULATOR_OK nessa etapa.
 */
int simulator_decode()
{
    bin_to_instruction(sim.ir, &(sim.decoded_instruction));
    return SIMULATOR_OK;
}

/**
 * @brief Etapa execute do simulador.
 *
 * Executa a etapa execute do simulador, onde a instrução decodificada é
 * executada.
 *
 * @return  Retorna um valor simulator_result. SIMULATOR_EXIT caso o programa
 *          tenha chegado ao fim, SIMULATOR_ERROR no caso de erro ou
 *          SIMULATOR_OK caso a execução deva continuar normalmente.
 */
int simulator_execute()
{
    //simulator_print_registers();          // DEBUG
    //simulator_print_memory(SECTION_DATA); // DEBUG
    return execute_instruction(&(sim.decoded_instruction));
}

/**
 * @brief Modifica o valor do PC.
 *
 * Modifica o valor do PC para o endereço dado. O endereço dado é um endereço
 * normal, em bytes. O endereço deve estar alinhado com words de 32 bits, ou
 * seja, ser divisível por 4. O valor no PC será o valor dado dividido por 4.
 *
 * @param address  O novo endereço (em bytes).
 *
 * @return         Retorna um simulator_result.
 */
int simulator_set_pc(uint32_t address)
{
    if(address % 4)
    {
        print_error("Jump para endereço não alinhado.");
        return SIMULATOR_ERROR;
    }
    sim.pc = address >> 2;
    return SIMULATOR_OK;
}

/**
 * @brief Modifica o PC para instruções jump.
 *
 * Modifica o valor do PC para o endereço dado, mantendo os últimos 6 bits do
 * PC, como é feito em instruções jump.
 *
 * @param address  O novo endereço, com 6 últimos bits iguais a 0.
 *
 * @return         Retorna um simulator_result.
 */
int simulator_jump_pc(uint32_t address)
{
    sim.pc &= 0xFC000000;
    sim.pc |= address;
    return SIMULATOR_OK;
}

/**
 * @brief Modifica o PC para instruções branch.
 *
 * Modifica o valor do PC, somando o endereço atual com o valor dado em words.
 *
 * @param offset_words  O valor, em words, a somar ao PC.
 *
 * @return              Retorna um simulator_result.
 */
int simulator_advance_pc(int32_t offset_words)
{
    sim.pc += offset_words;
    return SIMULATOR_OK;
}

/**
 * @brief Armazena o endereço atual no registrador 31.
 *
 * Armazena no registrador 31 o endereço atual do PC, em bytes, para ser usado
 * pela instrução jal.
 *
 * @return  Retorna um simulator_result.
 */
int simulator_link()
{
    sim.registers[31] = sim.pc << 2;
    return SIMULATOR_OK;
}

/**
 * @brief Lê um byte da memória de dados.
 *
 * Armazena em dest o valor do byte em address da memória de dados.
 *
 * @param address  Endereço a ser lido.
 * @param dest     Ponteiro para byte a receber o valor.
 *
 * @return         Retorna um simulator_result.
 */
int simulator_read_byte(uint32_t address, uint8_t *dest)
{
    uint8_t *mem = (uint8_t*)sim.data_memory;
    *dest = mem[address];
    return SIMULATOR_OK;
}

/**
 * @brief Armazena um byte na memória de dados.
 *
 * Armazena o byte dado no endereço address da memória de dados.
 *
 * @param address  Endereço para armazenar o byte.
 * @param byte     Byte com o valor a ser armazenado.
 *
 * @return         Retorna um simulator_result.
 */
int simulator_store_byte(uint32_t address, uint8_t byte)
{
    uint8_t *mem = (uint8_t*)sim.data_memory;
    mem[address] = byte;
    return SIMULATOR_OK;
}

/**
 * @brief Lê uma word da memória de dados.
 *
 * Armazena em dest a word no endereço address da memória de dados.
 *
 * @param address  Endereço a ser lido.
 * @param dest     Ponteiro para word a receber o valor.
 *
 * @return         Retorna um simulator_result.
 */
int simulator_read_word(uint32_t address, uint32_t *dest)
{
    *dest = sim.data_memory[address >> 2];
    return SIMULATOR_OK;
}

/**
 * @brief Armazena uma word na memória de dados.
 *
 * Armazena a word dada no endereço address da memória de dados.
 *
 * @param address  Endereço para armazenar a word.
 * @param byte     Word com o valor a ser armazenado.
 *
 * @return         Retorna um simulator_result.
 */
int simulator_store_word(uint32_t address, uint32_t word)
{
    sim.data_memory[address >> 2] = word;
    return SIMULATOR_OK;
}

/**
 * @brief Lê uma string da memória de dados.
 *
 * Aponta a string apontada por dest para endereço address da memória de dados.
 *
 * @param address  Endereço da string a ser lida.
 * @param dest     Ponteiro para string a receber o endereço da memória de dados.
 *
 * @return         Retorna um simulator_result.
 */
int simulator_get_string(uint32_t address, char **dest)
{
    char *mem = (char*)sim.data_memory;
    *dest = mem + address;
    return SIMULATOR_OK;
}

/**
 * @brief Armazena uma string na memória de dados.
 *
 * Armazena a string dada no endereço address da memória de dados.
 *
 * @param address  Endereço para armazenar a string.
 * @param n        Número máximo de bytes da string.
 * @param str      String a ser armazenada.
 *
 * @return         Retorna um simulator_result.
 */
int simulator_write_string(uint32_t address, size_t n, const char *str)
{
    char *dest = ((char*)sim.data_memory) + address;
    strncpy(dest, str, n);
    return SIMULATOR_OK;
}

/**
 * @brief Lê o valor de um registrador.
 *
 * Armazena em dest o valor do registrador reg.
 *
 * @param reg   Registrador a ser lido.
 * @param dest  Ponteiro para word a receber o valor do registrador.
 *
 * @return      Retorna um simulator_result.
 */
int simulator_get_reg(uint8_t reg, uint32_t *dest)
{
    *dest = sim.registers[reg];
    return SIMULATOR_OK;
}

/**
 * @brief Armazena um valor em um registrador.
 *
 * Armazena o valor value no registrador reg. Não permite armazenar um valor no
 * registrador zero.
 *
 * @param reg    Registrador a receber o valor.
 * @param value  Valor a ser armazenado no registrador.
 *
 * @return       Retorna um simulator_result.
 */
int simulator_set_reg(uint8_t reg, uint32_t value)
{
    if(reg > 0)
        sim.registers[reg] = value;
    return SIMULATOR_OK;
}

/**
 * @brief Lê um cabeçalho do executável.
 *
 * Lê um cabeçalho do executável. Os cabeçalhos são as linhas que começam com
 * .data ou .text e contém o tamanho da memória que deve ser alocada para cada
 * seção. O ponteiro passado como line_buffer deve apontar para o início de um
 * buffer com pelo menos IO_LINE_BUFFER_SIZE bytes.
 *
 * @param line_buffer  Ponteiro para início do buffer a receber linhas do arquivo.
 * @param s            Seção sendo lida.
 *
 * @return             Retorna 1 no caso de erro ou 0 se tudo correr bem.
 */
int read_header(char *line_buffer, enum simulator_section s)
{
    const char   data[] = ".data", text[] = ".text";
    const char  *header;
    uint32_t    *mem_size;
    uint32_t   **mem;
    if(s == SECTION_DATA)
    {
        header   = data;
        mem_size = &(sim.data_memory_size);
        mem      = &(sim.data_memory);
    }
    else if(s == SECTION_TEXT)
    {
        header   = text;
        mem_size = &(sim.instruction_memory_size);
        mem      = &(sim.instruction_memory);
    }
    else
        assert(0);

    char *tok;
    int   size;

    if(io_read_line_ne(line_buffer) == NULL)
    {
        print_error_ln("Fim insesperado do arquivo.");
        return 1;
    }
    tok = strtok(line_buffer, " \t\n");
    if(strcmp(tok, header) != 0)
    {
        print_error_ln_f("Formato inválido. Esperava %s.", header);
        return 1;
    }
    tok = strtok(NULL, " \t\n");
    if(sscanf(tok, "%d", &size) <= 0 || size <= 0)
    {
        print_error_ln_f("Tamanho para %s inválido.", header);
        return 1;
    }

    *mem_size = size;
    *mem      = malloc(4 * size);
    return 0;
}

/**
 * @brief Lê labels do executável.
 *
 * A parte de labels do executável é apenas para auxiliar na detecção de erros
 * do assembler, portanto ler os labels consiste simplesmente em ignorá-los, ou
 * seja, ignorar todas linhas que começam com '='.
 *
 * @param line_buffer  Ponteiro para início do buffer a receber linhas do arquivo.
 *
 * @return             Retorna 1 no caso de erro ou 0 se tudo correr bem.
 */
int read_labels(char *line_buffer)
{
    char c = '=';
    while(c == '=')
    {
        if(io_read_line_ne(line_buffer) == NULL)
        {
            print_error_ln("Fim insesperado do arquivo.");
            return 1;
        }
        sscanf(line_buffer, " %c", &c);
    }
    return 0;
}

/**
 * @brief Lê dados do executável.
 *
 * Lê a parte principal do executável, que é uma lista de words a ser colocada
 * na memória de dados ou de instruções.
 *
 * @param line_buffer  Ponteiro para início do buffer a receber linhas do arquivo.
 * @param s            Seção sendo lida.
 *
 * @return             Retorna 1 no caso de erro ou 0 se tudo correr bem.
 */
int read_data(char *line_buffer, enum simulator_section s)
{
    uint32_t *mem = (s == SECTION_DATA) ? sim.data_memory
                  : (s == SECTION_TEXT) ? sim.instruction_memory : NULL;
    assert(mem != NULL);

    int      pos  = 0;
    char     c    = 0;
    uint32_t word = 0x00000000;

    sscanf(line_buffer, " %c", &c);

    while(c != '.')
    {
        sscanf(line_buffer, "%"SCNx32, &word);
        mem[pos++] = word;
        if(io_read_line_ne(line_buffer) == NULL)
        {
            print_error_ln("Fim insesperado do arquivo.");
            return 1;
        }
        sscanf(line_buffer, " %c", &c);
    }
    return 0;
}

/**
 * @brief Lê um rodapé do executável.
 *
 * Lê o rodapé que encerra uma seção do arquivo executável, que deve conter ou
 * ".enddata" ou ".endtext", de acordo com a seção atual.
 *
 * @param line_buffer  Ponteiro para início do buffer a receber linhas do arquivo.
 * @param s            Seção sendo lida.
 *
 * @return             Retorna 1 no caso de erro ou 0 se tudo correr bem.
 */
int read_footer(char *line_buffer, enum simulator_section s)
{
    const char   data[] = ".enddata", text[] = ".endtext";
    const char  *footer = (s == SECTION_DATA) ? data
                        : (s == SECTION_TEXT) ? text : NULL;
    assert(footer != NULL);

    char *tok = strtok(line_buffer, " \t\n");
    if(strcmp(tok, footer) != 0)
    {
        print_error_ln_f("Formato inválido. Esperava %s.", footer);
        return 1;
    }
    return 0;
}

/**
 * @brief Imprime no stderr o valor de todos registradores.
 *
 * Imprime todos os registradores em stderr, para auxiliar o debug. Um possível
 * uso é dentro do próprio debugger, por exemplo, no gdb :
 * call simulator_print_registers()
 * irá imprimir o valor dos registradores em uma formatação fácil de ler.
 */
void simulator_print_registers()
{
    fprintf(stderr, " PC: %08"PRIX32"  IR: %08"PRIX32"\n", sim.pc, sim.ir);
    for(int i = 0; i < 32; i++)
    {
        fprintf(stderr, "$%2d: %08"PRIX32" ", i, sim.registers[i]);
        if(i % 4 == 3) fprintf(stderr, "\n");
    }
}

/**
 * @brief Imprime no stderr o dump de uma das memórias.
 *
 * Imprime uma memória inteira em stderr, para auxiliar o debug. Um possível
 * uso é dentro do próprio debugger, por exemplo, no gdb :
 * call simulator_print_memory(SECTION_DATA)
 * irá imprimir a memória de dados inteira.
 */
void simulator_print_memory(enum simulator_section s)
{
    const char data[] = "data", instr[] = "instruction", *name;
    uint32_t *mem, mem_size;

    switch(s)
    {
    case SECTION_DATA:
        mem      = sim.data_memory;
        mem_size = sim.data_memory_size;
        name     = data;
        break;
    case SECTION_TEXT:
        mem      = sim.instruction_memory;
        mem_size = sim.instruction_memory_size;
        name     = instr;
        break;
    default:
        return;
    }

    char *str = (char*)mem;
    char *end = str + (mem_size * 4);
    fprintf(stderr, "%s_memory:\n", name);
    for(int i = 0; i < mem_size; i++)
    {
        fprintf(stderr, "%08"PRIX32" ", mem[i]);

        if((i % 8 == 7) || (i == (mem_size - 1)))
        {
            for(int j = 0; j < 32; j++, str++)
            {
                if(str == end) break;

                char c = *str;
                if(isspace(c))
                    c = ' ';
                else if(!isprint(c))
                    c = '.';
                fprintf(stderr, "%c", c);
            }
            fprintf(stderr, "\n");
        }
    }
}

