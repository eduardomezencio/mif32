/**
 * @file   string_helper.c
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Definição de funções para tratamento de strings do assembler.
 *
 * Este arquivo define funções para tratamento de strings necessárias ao
 * assembler.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "string_helper.h"

/**
 * @brief Separa o comentário de uma linha de código.
 *
 * Separa o comentário de uma linha de código, colocando o endereço de início
 * do comentário no ponteiro apontado por comment, caso comment não seja NULL e
 * retornando um ponteiro para o resto do código.
 *
 * @param src     String com a linha de código.
 * @param comment Ponteiro que receberá a posição da string onde começa o
 *                comentário. Um caracter \0 será inserido antes desse ponto. Se
 *                este argumento for NULL, será ignorado.
 *
 * @return        Retorna ponteiro para o início da linha de código, com o
 *                comentário removido. O comentário ainda estará no vetor, mas
 *                um \0 será inserido entre o código e o comentário.
 */
char *sep_comment(char *src, char **comment)
{
    char *c = strchr(src, '#');

    if(c == NULL)
    {
        if(comment) *comment = NULL;
        return src;
    }

    c[0] = '\0';
    if(comment != NULL) *comment = c+1;
    return src;
}

/**
 * @brief Separa o label de uma linha de código.
 *
 * Separa o label de uma linha de código, colocando a posição de início do
 * label no ponteiro apontado pelo argumento label, se este não for NULL.
 * Retorna ponteiro para a posição de início da linha de código sem o label.
 * Deve ser usada em uma linha com os comentários já removidos.
 *
 * @param src     String com a linha de código.
 * @param label   Ponteiro que receberá a posição da string onde começa o
 *                label. Um caracter \0 será inserido onde o ':' estava. Se
 *                este argumento for NULL, será ignorado.
 *
 * @return        Retorna ponteiro para o início da linha de código, com o
 *                label removido. O label ainda estará no vetor, mas um \0 será
 *                inserido entre o label e o resto do código.
 */
char *sep_label(char *src, char **label)
{
    char *c = strchr(src, ':');

    if(c == NULL)
    {
        if(label) *label = NULL;
        return src;
    }

    c[0] = '\0';
    if(label != NULL) *label = src;
    return c + 1;
}

/**
 * @brief Separa os argumentos de uma linha de código.
 *
 * Separa os argumentos da instrução em uma linha de código. O ponto de início
 * dos argumentos na string é colocado no ponteiro apontado pelo argumento
 * args, caso este não seja NULL. Retorna um ponteiro para o ponto da string
 * onde começa a instrução. Deve ser usada em uma linha de código que já tenha
 * o comentário e o label removidos.
 *
 * @param src     String com a linha de código.
 * @param args    Ponteiro que receberá a posição da string onde começam os
 *                argumentos. Um caracter \0 será inserido antes desse ponto. Se
 *                este argumento for NULL, será ignorado.
 *
 * @return        Retorna ponteiro para o início da instrução, com os
 *                argumentos removidos. Se a função for usada depois de
 *                sep_comment e sep_label, é garantido que o nome da instrução
 *                não tenha espaços em branco ao redor. Se o argumento args não
 *                for NULL, receberá o ponto onde começam os argumentos.
 */
char *sep_args(char *src, char **args)
{
    char *i = strtok(src, " \t\n");
    if(args != NULL) *args = strtok(NULL, "\n");
    return i;
}

/**
 * @brief Retira espaços em branco das extremidades de uma string.
 *
 * Retira espaços em branco (detectados por @c isspace) do começo e final
 * da string passada como argumento e retorna um ponteiro para o início da
 * string com espaços removidos.
 *
 * @param src     String a ser tratada.
 *
 * @return        Retorna ponteiro para o início da string com espaços
 *                removidos.
 */
char *trim_space(char *src)
{
    while(isspace((unsigned char)src[0]))
        src++;

    char *end = src + strlen(src) - 1;
    while(isspace((unsigned char)end[0]) && (end > src))
        end--;
    end[1] = '\0';

    return src;
}

/**
 * @brief Insere string ao final de uma lista de strings.
 *
 * Insere uma cópia da string passada como argumento em uma lista de strings.
 *
 * @param l    Ponteiro para a lista de strings.
 * @param str  A string a ser inserida.
 */
void string_list_append(list_t *l, char *str)
{
    string_list_entry_t *s = malloc(sizeof(string_list_entry_t));
    s->str = malloc(strlen(str) + 1);
    strcpy(s->str, str);
    list_append(l, &(s->node));
}

/**
 * @brief Libera uma lista de strings
 *
 * Libera a memória alocada por uma lista de strings.
 *
 * @param l    Ponteiro para a lista de strings.
 */
void string_list_free(list_t *l)
{
    string_list_entry_t *pos, *n;
    list_for_each_entry_safe(l, pos, n, node)
    {
        free(pos->str);
        free(pos);
    }
}
