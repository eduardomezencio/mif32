/**
 * @file   mif32asm.c
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Definição da função main para o assembler.
 *
 * Este arquivo apenas define a função main. Dentro de main, são verificados os
 * parâmetros da linha de comando e então o assembler é chamado para fazer a
 * montagem.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#include <stdio.h>
#include <string.h>
#include "assembler.h"
#include "io.h"

int main(int argc, char **argv)
{
    if(argc < 2)
    {
        print_error("Parâmetros insuficientes.");

        char *cmd = strchr(argv[0], '/');
        cmd = (cmd == NULL) ? argv[0] : cmd + 1;
        print_syntax(cmd);

        return 1;
    }

    int error = argc < 3
        ? assembler_init(argv[1], "-")
        : assembler_init(argv[1], argv[2]);
    if(error) return 1;

    error = assembler_first_pass();
    if(error) return 1;

    error = assembler_second_pass();
    if(error) return 1;

    assembler_close();
    return 0;
}
