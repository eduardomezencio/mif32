/**
 * @file   instr_asm.c
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Definição das funções de montagem de instruções.
 *
 * Definição das funções do assembler para montagem de instruções.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#include <assert.h>
#include <ctype.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "instr_asm.h"
#include "io.h"
#include "label_list.h"
#include "string_helper.h"

/**
 * @brief Gera as instruções que substituem uma pseudo-instrução.
 *
 * Recebe como entrada informações sobre uma pseudo-instrução e gera uma lista
 * de strings, cada string correspondente a uma instrução. Essas instruções
 * substituem a pseudo-instrução dada.
 *
 * @param instr A entrada do dicionário da pseudo-instrução em questão.
 * @param args  Os argumentos passados para a pseudo instrução.
 * @param dest  A lista que receberá a saída. Deve estar vazia ou não iniciada.
 *
 * @return Retorna 0 se tudo correu bem ou 1 no caso de erro.
 */
int pseudo_expand(const dict_entry_t *instr, char *args, list_t *dest)
{
    assert(instr->type == 'p');

    char line_buffer[64], args_buffer[32];
    list_init(dest);

    // Lê os argumentos passados para a pseudo instrução e os coloca em arg
    int args_c = 0;
    char *arg[INSTR_MAX_ARGS];
    {
        int i = 0;
        for(char *c = strtok(args, ","); c != NULL; c = strtok(NULL, ","), i++)
        {
            uint32_t word;
            c = trim_space(c);
            switch(instr->info.p.arg_types[i])
            {
            case 'r': // Registrador
            case 'i': // Imediato
                arg[i] = malloc(strlen(c) + 1);
                strcpy(arg[i], c);
                break;
            case 'a': // Endereço (passado como label)
                word = get_label_address(c);
                if(word == LABEL_NOT_FOUND)
                {
                    for(int j = 0; j < i; free(arg[j++]));
                    return 1;
                }
                arg[i] = malloc(11);
                sprintf(arg[i], "%"PRIu32, word);
                break;
            default:
                assert(0);
            }

        }
        args_c = i;
        while(i < INSTR_MAX_ARGS) arg[i++] = NULL;
    }

    if(args_c != instr->info.p.arg_c)
    {
        print_error_ln_f("Número incorreto de argumentos para "
                         "pseudo-instrução %s.", instr->name);
        for(int i = 0; i < INSTR_MAX_ARGS; free(arg[i++]));
        return 1;
    }

    for(int i = 0; i < instr->info.p.instr_c; i++)
    {
        line_buffer[0] = '\0';
        strcat(line_buffer, instr->info.p.instr_names[i]);
        strcat(line_buffer, "\t");
        strcpy(args_buffer, instr->info.p.instr_args[i]);

        char sep[2];
        memset(sep, '\0', 2);
        for(char *c = strtok(args_buffer, ","); c != NULL; c = strtok(NULL, ","))
        {
            c = trim_space(c);
            strcat(line_buffer, sep);

            int n;
            uint32_t word;
            char arg_tmp[16];
            switch(c[0])
            {
            case 'a':
                sscanf(c, "a%d", &n);
                assert(n >= 0 && n < args_c);
                strcat(line_buffer, arg[n]);
                break;
            case 'l':
                sscanf(c, "l%d", &n);
                assert(n >= 0 && n < args_c);
                sscanf(arg[n], "%"SCNu32, &word);
                sprintf(arg_tmp, "%"PRIu32, (word & 0x0000FFFF));
                strcat(line_buffer, arg_tmp);
                break;
            case 'u':
                sscanf(c, "u%d", &n);
                assert(n >= 0 && n < args_c);
                sscanf(arg[n], "%"SCNu32, &word);
                sprintf(arg_tmp, "%"PRIu32, (word & 0xFFFF0000) >> 16);
                strcat(line_buffer, arg_tmp);
                break;
            default:
                strcat(line_buffer, c);
                break;
            }

            sep[0] = ',';
        }

        string_list_append(dest, line_buffer);
    }

    for(int i = 0; i < INSTR_MAX_ARGS; free(arg[i++]));
    return 0;
}

/**
 * @brief Codifica uma instrução.
 *
 * Recebe como entrada informações sobre a instrução codifica essa instrução
 * em uma @c struct @c instruction.
 *
 * @param instr    A entrada do dicionário da instrução em questão.
 * @param args     Os argumentos passados para a instrução.
 * @param ref_addr O endereço da instrução no código, para calcular os saltos
 *                 em instruções de branch e jump.
 * @param dest     A @struct @instruction que receberá a instrução.
 *
 * @return Retorna 0 se tudo correu bem ou 1 no caso de erro.
 */
int instruction_encode(const dict_entry_t *instr, char *args,
                       uint32_t ref_addr, instruction_t *dest)
{
    assert(instr->type == 'i');

    uint8_t  rs = 0x00, rt = 0x00, rd = 0x00, shamt = 0x00;
    uint16_t immediate = 0x0000;
    uint32_t address   = 0x00000000;

    int i = 0;
    for(char *c = strtok(args, ","); c != NULL; c = strtok(NULL, ","))
    {
        uint32_t tmp32;
        c = trim_space(c);

        instr_arg_t a = instr->info.i.args[i++];
        switch(a)
        {
        case arg_none:
            print_error_ln_f("Argumentos excedentes para instrução %s.",instr->name);
            return 1;
        case arg_rs:
            rs = register_to_bin(c);
            if(rs & REGISTER_ERROR) return 1;
            break;
        case arg_rt:
            rt = register_to_bin(c);
            if(rt & REGISTER_ERROR) return 1;
            break;
        case arg_rd:
            rd = register_to_bin(c);
            if(rd & REGISTER_ERROR) return 1;
            break;
        case arg_immed:
            tmp32 = immediate_to_bin(c, arg_immed);
            if(tmp32 == IMMEDIATE_ERROR) return 1;
            immediate = (uint16_t)tmp32;
            break;
        case arg_immed_offset:
            tmp32 = immediate_to_bin(c, arg_immed_offset);
            if(tmp32 == IMMEDIATE_ERROR) return 1;
            immediate = (uint16_t)tmp32;
            rs        = ((uint8_t *)(&tmp32))[2];
            break;
        case arg_label:
            tmp32 = label_to_bin(c, instr->info.i.format, ref_addr);
            if(tmp32 == LABEL_ERROR) return 1;
            immediate = (uint16_t)tmp32;
            address   = tmp32;
            break;
        case arg_shamt:
            tmp32 = immediate_to_bin(c, arg_shamt);
            if(tmp32 == IMMEDIATE_ERROR) return 1;
            shamt = ((uint8_t*)(&tmp32))[0];
            break;
        }
    }
    if(i < INSTR_MAX_ARGS && instr->info.i.args[i] != arg_none)
    {
        print_error_ln_f("Argumentos insuficientes para instrução %s.",instr->name);
        return 1;
    }

    char format = instr->info.i.format;
    dest->format = format;

    switch(format)
    {
    case 'R':
        dest->data.r.rs        = rs;
        dest->data.r.rt        = rt;
        dest->data.r.rd        = rd;
        dest->data.r.shamt     = shamt;
        dest->data.r.funct     = instr->info.i.opcode;
        break;
    case 'I':
        dest->data.i.opcode    = instr->info.i.opcode;
        dest->data.i.rs        = rs;
        dest->data.i.rt        = rt;
        dest->data.i.immediate = immediate;
        break;
    case 'J':
        dest->data.j.opcode    = instr->info.i.opcode;
        dest->data.j.address   = address;
        break;
    default:
        assert(0);
    }

    return 0;
}

/**
 * @brief Gera a representação numérica de um registrador a partir de uma string.
 *
 * Lê uma string que contém um registrador, da forma como foi passado como
 * argumento a uma instrução, e converte no código numérico de 5 bits do
 * registrador em questão.
 *
 * @param reg  A string com o nome do registrador.
 *
 * @return Retorna o código numérico de 5 bits do registrador, ou
 *         REGISTER_ERROR no caso de erro.
 */
uint8_t register_to_bin(const char *reg)
{
    uint8_t bin, i;

    if(reg[0] != '$')
    { }
    else if(isdigit(reg[1]))
    {
        sscanf(reg, "$%"SCNu8, &bin);
        if(bin < 32) return bin;
    }
    else
    {
        switch(reg[1])
        {
        case 'z':
            if(strcmp(reg, "$zero") == 0)        return 0;
            break;
        case 'a':
            if(strcmp(reg, "$at") == 0)          return 1;
            if(sscanf(reg, "$a%"SCNu8, &i) <= 0) break;
            if(i < 4)                            return 4 + i;
            break;
        case 'v':
            if(sscanf(reg, "$v%"SCNu8, &i) <= 0) break;
            if(i < 2)                            return 2 + i;
            break;
        case 't':
            if(sscanf(reg, "$t%"SCNu8, &i) <= 0) break;
            if(i < 8)                            return 8 + i;
            if(i < 10)                           return 24 + i;
            break;
        case 's':
            if(strcmp(reg, "$sp") == 0)          return 29;
            if(sscanf(reg, "$s%"SCNu8, &i) <= 0) break;
            if(i < 8)                            return 16 + i;
            break;
        case 'k':
            if(sscanf(reg, "$k%"SCNu8, &i) <= 0) break;
            if(i < 2)                            return 26 + i;
            break;
        default:
            if(strcmp(reg, "$gp") == 0)          return 28;
            if(strcmp(reg, "$fp") == 0)          return 30;
            if(strcmp(reg, "$ra") == 0)          return 31;
            break;
        }
    }

    print_error_ln_f("Registrador %s inválido.", reg);
    return REGISTER_ERROR;
}

/**
 * @brief Gera a representação numérica de um valor imediato.
 *
 * Lê uma string que contém um valor imediato, da forma como foi passado como
 * argumento a uma instrução, e converte no código numérico que será usado para
 * compor a representação binária da instrução. Pode ser usada para valores
 * imediatos, valores imediatos com offset (C($s) <- o valor imediato e o
 * registrador entre parenteses contam como um argumento apenas neste assembler),
 * e para valores de shift_ammount. No caso de um imediato com offset, retorna
 * ambos em uma word, com o imediato nos 16 bits menos significativos e o
 * registrador nos próximos 8 bits.
 *
 * @param immed  A string com o valor imediato.
 * @param argt   O tipo do argumento
 *
 * @return Retorna o código numérico do valor imediato. No caso de um imediato
 *         com offset, os bits 16-23 contém o registrador. No caso de erro, o
 *         valor REGISTER_ERROR é retornado.
 */
uint32_t immediate_to_bin(const char *immed, instr_arg_t argt)
{
    uint32_t bin = 0x00000000;
    int32_t  tmp32;
    char     reg[INSTR_MAX_ARG_SIZE+1];

    int read = sscanf(immed, "%"SCNd32"( %[^) ] )", &tmp32, reg);

    if(read <= 0 || tmp32 > INT16_MAX || tmp32 < INT16_MIN
                 || (argt == arg_shamt && (tmp32 > 31 || tmp32 < 0)))
    {
        const char i[] = "imediato", s[] = "shift";
        print_error_ln_f("Valor %s %s inválido",
                          (argt == arg_shamt) ? s : i, immed);
        return IMMEDIATE_ERROR;
    }
    bin = (uint32_t)tmp32;
    if(argt != arg_immed_offset) return bin;

    uint8_t tmp8 = (read < 2) ? REGISTER_ERROR : register_to_bin(reg);
    if(tmp8 & REGISTER_ERROR) return IMMEDIATE_ERROR;
    bin |= (tmp8 << 16);
    return bin;
}

/**
 * @brief Gera a representação numérica de um label.
 *
 * Lê uma string que contém um label e converte no código numérico que será
 * usado para compor a representação binária da instrução. O valor é diferente
 * para instruções de branch e instruções de jump. Nas instruções de branch o
 * valor é de 16 bits e é relativo ao endereço da instrução e nas instruções de
 * jump o valor consiste nos bits 2-27 do endereço absoluto do label.
 *
 * @param label    A string com o nome do label.
 * @param format   O formato da instrução ('I' ou 'J').
 * @param ref_addr O endereço da instrução.
 *
 * @return Retorna o código numérico correspondente ao label. No caso de uma
 *         instrução de branch, um valor de 16 bits que é relativo ao endereço
 *         da instrução. No caso de uma instrução de jump um valor de 26 bits
 *         com os 28 bits menos significativos do endereço, tirando os dois
 *         primeiros bits, que são sempre zero. Retorna LABEL_ERROR no caso de
 *         erro.
 */
uint32_t label_to_bin(const char *label, char format, uint32_t ref_addr)
{
    uint32_t address = get_label_address(label);
    if(address == LABEL_NOT_FOUND) return LABEL_ERROR;

    union {int32_t s; uint32_t u;} bin;
    ref_addr += 4;

    switch(format)
    {
    case 'I':
        bin.s = (int32_t)(address - ref_addr);
        assert(!(bin.s % 4));
        bin.s = bin.s / 4;
        if(bin.s > INT16_MAX || bin.s < INT16_MIN)
        {
            print_error_ln_f("Label %s muito distante para branch.", label);
            return LABEL_ERROR;
        }
        return bin.s;
    case 'J':
        bin.u = address & 0x0FFFFFFF;
        assert(!(bin.u % 4));
        bin.u = bin.u / 4;
        if((address & 0xF0000000) != (ref_addr & 0xF0000000))
        {
            print_error_ln_f("Label %s muito distante para jump.", label);
            return LABEL_ERROR;
        }
        return bin.u;
    }

    assert(0);
}

