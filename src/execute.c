/**
 * @file   execute.c
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Definição de funções para simulação das instruções.
 *
 * Este arquivo define as funções que simulam a execução de cada instrução.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "execute.h"
#include "io.h"
#include "simulator.h"

// Vetor indexado pelo valor funct de cada instrução do formato R
int (*exec_r_table[])(const instruction_t *) =
{
    &execute_sll,     /* 0x00 */    NULL,             /* 0x01 */
    &execute_srl,     /* 0x02 */    &execute_sra,     /* 0x03 */
    &execute_sllv,    /* 0x04 */    NULL,             /* 0x05 */
    &execute_srlv,    /* 0x06 */    &execute_srav,    /* 0x07 */
    &execute_jr,      /* 0x08 */    NULL,             /* 0x09 */
    NULL,             /* 0x0A */    NULL,             /* 0x0B */
    &execute_syscall, /* 0x0C */    NULL,             /* 0x0D */
    NULL,             /* 0x0E */    NULL,             /* 0x0F */
    &execute_mfhi,    /* 0x10 */    NULL,             /* 0x11 */
    &execute_mflo,    /* 0x12 */    NULL,             /* 0x13 */
    NULL,             /* 0x14 */    NULL,             /* 0x15 */
    NULL,             /* 0x16 */    NULL,             /* 0x17 */
    &execute_mult,    /* 0x18 */    &execute_multu,   /* 0x19 */
    &execute_div,     /* 0x1A */    &execute_divu,    /* 0x1B */
    NULL,             /* 0x1C */    NULL,             /* 0x1D */
    NULL,             /* 0x1E */    NULL,             /* 0x1F */
    &execute_add,     /* 0x20 */    &execute_addu,    /* 0x21 */
    &execute_sub,     /* 0x22 */    &execute_subu,    /* 0x23 */
    &execute_and,     /* 0x24 */    &execute_or,      /* 0x25 */
    &execute_xor,     /* 0x26 */    &execute_nor,     /* 0x27 */
    NULL,             /* 0x28 */    NULL,             /* 0x29 */
    &execute_slt      /* 0x2A */
};

// Vetor indexado pelo opcode de cada instrução dos formatos I e J
int (*exec_ij_table[])(const instruction_t *) =
{
    NULL,             /* 0x00 */    &execute_bltz,    /* 0x01 */
    &execute_j,       /* 0x02 */    &execute_jal,     /* 0x03 */
    &execute_beq,     /* 0x04 */    &execute_bne,     /* 0x05 */
    NULL,             /* 0x06 */    NULL,             /* 0x07 */
    &execute_addi,    /* 0x08 */    &execute_addiu,   /* 0x09 */
    &execute_slti,    /* 0x0A */    NULL,             /* 0x0B */
    &execute_andi,    /* 0x0C */    &execute_ori,     /* 0x0D */
    &execute_xori,    /* 0x0E */    &execute_lui,     /* 0x0F */
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,   /* 0x10 - 0x17 */
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,   /* 0x18 - 0x1F */
    &execute_lb,      /* 0x20 */    NULL,             /* 0x21 */
    NULL,             /* 0x22 */    &execute_lw,      /* 0x23 */
    &execute_lbu,     /* 0x24 */    NULL,             /* 0x25 */
    NULL,             /* 0x26 */    NULL,             /* 0x27 */
    &execute_sb,      /* 0x28 */    NULL,             /* 0x29 */
    NULL,             /* 0x2A */    &execute_sw       /* 0x2B */
};

static inline int      check_overflow (uint32_t op1, uint32_t op2, uint32_t result);
static inline uint32_t sign_extend_8  (uint32_t value);
static inline uint32_t sign_extend_16 (uint32_t value);

int syscall_print_integer     (uint32_t code, uint32_t *args);
int syscall_print_string      (uint32_t code, uint32_t *args);
int syscall_read_integer      (uint32_t code, uint32_t *args);
int syscall_read_string       (uint32_t code, uint32_t *args);
int syscall_print_character   (uint32_t code, uint32_t *args);
int syscall_read_character    (uint32_t code, uint32_t *args);
int syscall_print_integer_hex (uint32_t code, uint32_t *args);

/**
 * @brief Executa uma instrução.
 *
 * @param instr  Instrução a ser executada.
 *
 * @return       Retorna um simulator_result.
 */
int execute_instruction(const instruction_t *instr)
{
    switch(instr->format)
    {
    case 'R':
        return exec_r_table[instr->data.r.funct](instr);
    case 'I':
        return exec_ij_table[instr->data.i.opcode](instr);
    case 'J':
        return exec_ij_table[instr->data.j.opcode](instr);
    }
    assert(0);
}

int execute_addi(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "addi");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.i.rs, &op1);
    op2 = sign_extend_16(instr->data.i.immediate);
    result = op1 + op2;
    simulator_set_reg(instr->data.i.rt, result);
    return check_overflow(op1, op2, result);
}

int execute_addiu(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "addiu");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.i.rs, &op1);
    op2 = sign_extend_16(instr->data.i.immediate);
    result = op1 + op2;
    simulator_set_reg(instr->data.i.rt, result);
    return SIMULATOR_OK;
}

int execute_add(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "add");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.r.rs, &op1);
    simulator_get_reg(instr->data.r.rt, &op2);
    result = op1 + op2;
    simulator_set_reg(instr->data.r.rd, result);
    return check_overflow(op1, op2, result);
}

int execute_addu(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "addu");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.r.rs, &op1);
    simulator_get_reg(instr->data.r.rt, &op2);
    result = op1 + op2;
    simulator_set_reg(instr->data.r.rd, result);
    return SIMULATOR_OK;
}

int execute_andi(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "andi");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.i.rs, &op1);
    op2 = sign_extend_16(instr->data.i.immediate);
    result = op1 & op2;
    simulator_set_reg(instr->data.i.rt, result);
    return SIMULATOR_OK;
}

int execute_and(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "and");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.r.rs, &op1);
    simulator_get_reg(instr->data.r.rt, &op2);
    result = op1 & op2;
    simulator_set_reg(instr->data.r.rd, result);
    return SIMULATOR_OK;
}

int execute_beq(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "beq");
    uint32_t op1, op2;
    simulator_get_reg(instr->data.i.rs, &op1);
    simulator_get_reg(instr->data.i.rt, &op2);
    if(op1 == op2)
    {
        uint32_t offset = sign_extend_16(instr->data.i.immediate);
        simulator_advance_pc(offset);
    }
    return SIMULATOR_OK;
}

int execute_bltz(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "bltz");
    uint32_t op1;
    simulator_get_reg(instr->data.i.rs, &op1);
    if(op1 >= 0x80000000)
    {
        uint32_t offset = sign_extend_16(instr->data.i.immediate);
        simulator_advance_pc(offset);
    }
    return SIMULATOR_OK;
}

int execute_bne(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "bne");
    uint32_t op1, op2;
    simulator_get_reg(instr->data.i.rs, &op1);
    simulator_get_reg(instr->data.i.rt, &op2);
    if(op1 != op2)
    {
        uint32_t offset = sign_extend_16(instr->data.i.immediate);
        simulator_advance_pc(offset);
    }
    return SIMULATOR_OK;
}

int execute_div(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "div");
    print_error("Instrução div não implementada.");
    return SIMULATOR_ERROR;
}

int execute_divu(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "divu");
    print_error("Instrução divu não implementada.");
    return SIMULATOR_ERROR;
}

int execute_jal(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "jal");
    simulator_link();
    simulator_jump_pc(instr->data.j.address);
    return SIMULATOR_OK;
}

int execute_j(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "j");
    simulator_jump_pc(instr->data.j.address);
    return SIMULATOR_OK;
}

int execute_jr(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "jr");
    uint32_t address;
    simulator_get_reg(instr->data.r.rs, &address);
    return simulator_set_pc(address);
}

int execute_lb(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "lb");
    uint8_t  value_8;
    uint32_t address, value;
    simulator_get_reg(instr->data.i.rs, &address);
    address += instr->data.i.immediate;
    simulator_read_byte(address, &value_8);
    value = value_8;
    value = sign_extend_8(value);
    simulator_set_reg(instr->data.i.rt, value);
    return SIMULATOR_OK;
}

int execute_lbu(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "lbu");
    uint8_t  value_8;
    uint32_t address, value;
    simulator_get_reg(instr->data.i.rs, &address);
    address += instr->data.i.immediate;
    simulator_read_byte(address, &value_8);
    value = value_8;
    simulator_set_reg(instr->data.i.rt, value);
    return SIMULATOR_OK;
}

int execute_lui(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "lui");
    uint32_t value = instr->data.i.immediate << 16;
    simulator_set_reg(instr->data.i.rt, value);

    return SIMULATOR_OK;
}

int execute_lw(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "lw");
    uint32_t address, value;
    simulator_get_reg(instr->data.i.rs, &address);
    address += instr->data.i.immediate;
    simulator_read_word(address, &value);
    simulator_set_reg(instr->data.i.rt, value);
    return SIMULATOR_OK;
}

int execute_mfhi(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "mfhi");
    print_error("Instrução mfhi não implementada.");
    return SIMULATOR_ERROR;
}

int execute_mflo(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "mflo");
    print_error("Instrução mflo não implementada.");
    return SIMULATOR_ERROR;
}

int execute_mult(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "mult");
    print_error("Instrução mult não implementada.");
    return SIMULATOR_ERROR;
}

int execute_multu(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "multu");
    print_error("Instrução multu não implementada.");
    return SIMULATOR_ERROR;
}

int execute_nor(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "nor");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.r.rs, &op1);
    simulator_get_reg(instr->data.r.rt, &op2);
    result = ~(op1 | op2);
    simulator_set_reg(instr->data.r.rd, result);
    return SIMULATOR_OK;
}

int execute_ori(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "ori");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.i.rs, &op1);
    op2 = sign_extend_16(instr->data.i.immediate);
    result = op1 | op2;
    simulator_set_reg(instr->data.i.rt, result);
    return SIMULATOR_OK;
}

int execute_or(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "or");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.r.rs, &op1);
    simulator_get_reg(instr->data.r.rt, &op2);
    result = op1 | op2;
    simulator_set_reg(instr->data.r.rd, result);
    return SIMULATOR_OK;
}

int execute_sb(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "sb");
    uint8_t  value_8;
    uint32_t address, value;
    simulator_get_reg(instr->data.i.rs, &address);
    address += instr->data.i.immediate;
    simulator_get_reg(instr->data.i.rt, &value);
    value_8 = (uint8_t)(value & 0xFF);
    simulator_store_byte(address, value_8);
    return SIMULATOR_OK;
}

int execute_sll(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "sll");
    uint32_t value, shift;
    simulator_get_reg(instr->data.r.rt, &value);
    shift = instr->data.r.shamt;
    value = value << shift;
    simulator_set_reg(instr->data.r.rd, value);
    return SIMULATOR_OK;
}

int execute_sllv(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "sllv");
    uint32_t value, shift;
    simulator_get_reg(instr->data.r.rt, &value);
    simulator_get_reg(instr->data.r.rs, &shift);
    value = value << shift;
    simulator_set_reg(instr->data.r.rd, value);
    return SIMULATOR_OK;
}

int execute_slti(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "slti");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.i.rs, &op1);
    op2 = sign_extend_16(instr->data.i.immediate);
    result = op1 < op2;
    simulator_set_reg(instr->data.i.rt, result);
    return SIMULATOR_OK;
}

int execute_slt(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "slt");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.r.rs, &op1);
    simulator_get_reg(instr->data.r.rt, &op2);
    result = op1 < op2;
    simulator_set_reg(instr->data.r.rd, result);
    return SIMULATOR_OK;
}

int execute_sra(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "sra");
    uint32_t value, shift;
    simulator_get_reg(instr->data.r.rt, &value);
    shift = instr->data.r.shamt;
    value = ((int32_t)value) >> shift;
    simulator_set_reg(instr->data.r.rd, value);
    return SIMULATOR_OK;
}

int execute_srav(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "srav");
    uint32_t value, shift;
    simulator_get_reg(instr->data.r.rt, &value);
    simulator_get_reg(instr->data.r.rs, &shift);
    value = ((int32_t)value) >> shift;
    simulator_set_reg(instr->data.r.rd, value);
    return SIMULATOR_OK;
}

int execute_srl(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "srl");
    uint32_t value, shift;
    simulator_get_reg(instr->data.r.rt, &value);
    shift = instr->data.r.shamt;
    value = value >> shift;
    simulator_set_reg(instr->data.r.rd, value);
    return SIMULATOR_OK;
}

int execute_srlv(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "srlv");
    uint32_t value, shift;
    simulator_get_reg(instr->data.r.rt, &value);
    simulator_get_reg(instr->data.r.rs, &shift);
    value = value >> shift;
    simulator_set_reg(instr->data.r.rd, value);
    return SIMULATOR_OK;
}

int execute_sub(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "sub");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.r.rs, &op1);
    simulator_get_reg(instr->data.r.rt, &op2);
    op2 = -((int32_t)op2);
    result = op1 + op2;
    simulator_set_reg(instr->data.r.rd, result);
    return check_overflow(op1, op2, result);
}

int execute_subu(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "subu");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.r.rs, &op1);
    simulator_get_reg(instr->data.r.rt, &op2);
    result = op1 - op2;
    simulator_set_reg(instr->data.r.rd, result);
    return SIMULATOR_OK;
}

int execute_sw(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "sw");
    uint32_t address, value;
    simulator_get_reg(instr->data.i.rs, &address);
    address += instr->data.i.immediate;
    simulator_get_reg(instr->data.i.rt, &value);
    simulator_store_word(address, value);
    return SIMULATOR_OK;
}

int execute_syscall(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "syscall");
    uint32_t code, args[3];
    simulator_get_reg(2, &code);
    simulator_get_reg(4, args);
    simulator_get_reg(5, args + 1);
    simulator_get_reg(6, args + 2);

    switch(code)
    {
    case 1:  return syscall_print_integer     (code, args);
    case 4:  return syscall_print_string      (code, args);
    case 5:  return syscall_read_integer      (code, args);
    case 8:  return syscall_read_string       (code, args);
    case 10: return SIMULATOR_EXIT;
    case 11: return syscall_print_character   (code, args);
    case 12: return syscall_read_character    (code, args);
    case 34: return syscall_print_integer_hex (code, args);
    }

    print_error_f("Serviço %"PRIu32" do syscall não implementado.", code);
    return SIMULATOR_ERROR;
}

int execute_xori(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "xori");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.i.rs, &op1);
    op2 = sign_extend_16(instr->data.i.immediate);
    result = op1 ^ op2;
    simulator_set_reg(instr->data.i.rt, result);
    return SIMULATOR_OK;
}

int execute_xor(const instruction_t *instr)
{
    //fprintf(stderr, "--%s--\n", "xor");
    uint32_t op1, op2, result;
    simulator_get_reg(instr->data.r.rs, &op1);
    simulator_get_reg(instr->data.r.rt, &op2);
    result = op1 ^ op2;
    simulator_set_reg(instr->data.r.rd, result);
    return SIMULATOR_OK;
}

/**
 * @brief Checa se uma operação causou overflow.
 *
 * Recebe os operadores e o resultado de uma operação e determina se ocorreu
 * overflow, retornando SIMULATOR_ERROR caso tenha ocorrido.
 *
 * @return  Retorna um simulator_result. SIMULATOR_ERROR se houver overflow e
 *          SIMULATOR_OK caso contrário.
 */
static inline int check_overflow(uint32_t op1, uint32_t op2, uint32_t result)
{
    if((op1 <  0x80000000 && op2 <  0x80000000 && result >= 0x80000000) ||
       (op1 >= 0x80000000 && op2 >= 0x80000000 && result <  0x80000000))
    {
        print_error("Overflow exception. Abortando.");
        return SIMULATOR_ERROR;
    }
    return SIMULATOR_OK;
}

/**
 * @brief Faz sign extension de um número de 8 bits para 32 bits.
 *
 * @param value  O valor a ser extendido.
 *
 * @return       Retorna o valor passado sign-extended para 32 bits.
 */
static inline uint32_t sign_extend_8(uint32_t value)
{
    if(value & 0x00000080)
        return value |= 0xFFFFFF00;
    return value;
}

/**
 * @brief Faz sign extension de um número de 16 bits para 32 bits.
 *
 * @param value  O valor a ser extendido.
 *
 * @return       Retorna o valor passado sign-extended para 32 bits.
 */
static inline uint32_t sign_extend_16(uint32_t value)
{
    if(value & 0x00008000)
        return value |= 0xFFFF0000;
    return value;
}

int syscall_print_integer(uint32_t code, uint32_t *args)
{
    char str[16];
    sprintf(str, "%"PRId32, args[0]);
    io_write_string(str);
    return SIMULATOR_OK;
}

int syscall_print_string(uint32_t code, uint32_t *args)
{
    char *str;
    simulator_get_string(args[0], &str);
    io_write_string(str);
    return SIMULATOR_OK;
}

int syscall_read_integer(uint32_t code, uint32_t *args)
{
    char str[IO_LINE_BUFFER_SIZE];
    io_read_line_ne(str);
    int32_t num;
    if(sscanf(str, " %"SCNd32, &num) > 0)
    {
        simulator_set_reg(2, (uint32_t)num);
        return SIMULATOR_OK;
    }
    print_error("Falha ao ler número inteiro.");
    return SIMULATOR_ERROR;
}

int syscall_read_string(uint32_t code, uint32_t *args)
{
    char str[IO_LINE_BUFFER_SIZE];
    io_read_line_ne(str);
    simulator_write_string(args[0], args[1], str);
    return SIMULATOR_OK;
}

int syscall_print_character(uint32_t code, uint32_t *args)
{
    char str[2];
    sprintf(str, "%c", (char)args[0]);
    io_write_string(str);
    return SIMULATOR_OK;
}

int syscall_read_character(uint32_t code, uint32_t *args)
{
    char str[IO_LINE_BUFFER_SIZE], c;
    io_read_line_ne(str);
    sscanf(str, " %c", &c);
    simulator_set_reg(2, (uint32_t)c);
    return SIMULATOR_OK;
}

int syscall_print_integer_hex(uint32_t code, uint32_t *args)
{
    char str[16];
    sprintf(str, "%"PRIX32, args[0]);
    io_write_string(str);
    return SIMULATOR_OK;
}

