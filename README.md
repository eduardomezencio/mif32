# MIF32

por *Eduardo Mezêncio*

Assembler(**mif32asm**) e simulador(**mif32sim**) para a arquitetura fictícia
MIPS-IF32.

## Arquitetura

A arquitetura MIPS-IF32 é baseada na arquitetura MIPS, porém simplificada para
fins didáticos.

* As instruções de multiplicação e divisão, assim como os registradores HI e LO
não são implementados.
* O acesso à memória é sempre alinhado a words de 32 bits (4 bytes).
* Possui memória de dados e memória de instruções separadas.
* Todas instruções executam em um ciclo (livre de delay slot).

### Instruções

* Formato R:
  add, addu, and, jr, nor, or, sll, sllv, slt, sra, srav, srl, srlv, sub, subu,
  syscall, xor
  
* Formato I:
  addi, addiu, andi, beq, bltz, bne, lb, lbu, lui, lw, ori, sb, slti, sw, xori
  
* Formato J:
  jal, j

## Compilando

Para compilar, basta rodar o comando:

    make
    
O assembler e o simulador podem ser compilados separadamente:

    make mif32asm
    make mif32sim

Para gerar a documentação Doxygen, rodar o comando:

    doxygen Doxyfile


## Assembler (mif32asm)

Para fazer a montagem de um código, rodar o assembler passando o nome do arquivo
de código pela linha de comando:

    ./mif32asm codigo.asm

Se for passado um segundo argumento na linha de comando, este será o arquivo de
saída. Caso não seja passado esse argumento, como no exemplo acima, a saída será
na saída padrão (stdout). Através da saída padrão pode-se conectar a saída do
assembler na entrada do simulador, para executar um código sem gerar arquivos
intermediários. Isso é demonstrado abaixo, na seção do simulador.

O assembler depende de dois arquivos, instr e pseudo, onde estão as informações
sobre as instruções e pseudo-instruções suportadas. Estes arquivos estão em um
formato de texto plano, permitindo o acréscimo facilitado de novas instruções
e pseudo-instruções. Estes arquivos são buscados na pasta onde o programa é
executado.

O assembler é limitado a códigos que consistem em uma seção .data seguida de uma
seção .text, não suportando a ordem inversa ou múltiplas seções data ou text.

A saída do assembler é em um formato de texto que é reconhecido pelo simulador.
O formato inclui o tamanho da memória de dados e da memória de instruções que
o simulador deve alocar e também inclui uma lista dos labels com seus endereços,
que não é utilizada pelo simulador, mas apenas para auxiliar na detecção de
erros.

## Simulador (mif32sim)

Para rodar um 'executável', basta rodar o simulador passando o nome do arquivo
a ser executado pela linha de comando:

    ./mif32sim executavel

O 'executável' seria o arquivo de texto gerado pelo assembler. Também é possível
rodar a partir da entrada padrão (stdin), de forma que a entrada pode vir
diretamente da saída do assembler. Para isso, passar o caracter '-' como
argumento de linha de comando para o programa:

    ./mif32asm codigo.asm | ./mif32sim -

Esse método irá falhar caso o programa a ser executado espere algum tipo de
entrada durante a execução. Para resolver este problema, a entrada esperada
também deve ser passada pelo pipe. Exemplo de programa que espera a entrada de
um número inteiro:

    ( ./mif32asm test/op.asm; echo 21 ) | ./mif32sim -

Isso irá rodar o programa test/op.asm passando como entrada o número 21.

## Licença

GPLv3. Ver o arquivo LICENSE.

