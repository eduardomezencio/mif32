/**
 * @file   list.h
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Implementação de lista encadeada genérica.
 *
 * Lista inspirada pela implementação de lista usada no kernel do linux.
 * Como as funções são todas curtas, todas são declaradas e definidas neste
 * arquivo como static inline, para resultar em código mais eficiente. Alguns
 * métodos precisam ser definidos por macros para possibilitar o uso de tipos
 * variados ou para simplificar a iteração pela lista.
 *
 * @see https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git/tree/include/linux/list.h?id=refs/tags/v4.10.12
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#ifndef INCLUDE_LIST_H
#define INCLUDE_LIST_H

/**
 * @brief Nó da lista encadeada.
 *
 * O struct list_node representa um nó da lista. Os nós da lista não contém os
 * dados, esses devem ser acessados pela macro @c list_entry. Para que seja
 * possível criar uma lista de um certo @c struct este deve ter um membro do
 * tipo @c list_node para representar sua posição na lista.
 *
 * @see list_entry
 */
struct list_node
{
    struct list_node *next; /**< Ponteiro para o próximo nó da lista. */
};

/**
 * @brief Lista encadeada genérica.
 *
 * A lista consiste apenas em um nó sentinela que aponta para o primeiro
 * elemento da lista e um ponteiro que aponta para o último. O último elemento
 * aponta de volta para o nó sentinela.
 */
struct list
{
    struct list_node  head; /**< Nó sentinela da lista. */
    struct list_node *last; /**< Ponteiro para o último nó da lista.
                                 last->next deve sempre apontar para head. */
};

typedef struct list_node  list_node_t;
typedef struct list       list_t;

/**
 * @brief Inicia uma lista vazia.
 *
 * A lista será iniciada com ambos @c head.next e @c last apontando para
 * @c head, sendo este o estado que representa uma lista vazia.
 *
 * @param l Ponteiro para a lista.
*/
static inline void list_init(list_t *l)
{
    l->head.next = l->last = &(l->head);
}

/**
 * @brief Insere nó após dada posição da lista.
 *
 * Insere o nó passado como argumento @c node após o nó passado em @c pos.
 *
 * @param l     Ponteiro para a lista.
 * @param node  Ponteiro para o nó a ser inserido.
 * @param pos   Ponteiro para o nó anterior ao ponto de inserção.
 */
static inline void list_insert(list_t *l, list_node_t *node, list_node_t *pos)
{
    if(pos == l->last)
        l->last = node;

    node->next = pos->next;
    pos->next  = node;
}

/**
 * @brief Insere nó no final da lista.
 *
 * Insere o nó passado como argumento @c node ao final da lista. Pode ser usado
 * para enfileirar, se quiser usar a lista como uma fila.
 *
 * @param l     Ponteiro para a lista.
 * @param node  Ponteiro para o nó a ser inserido.
 */
static inline void list_append(list_t *l, list_node_t *node)
{
    l->last->next = node;
    l->last       = node;
    node->next    = &(l->head);
}

/**
 * @brief Insere nó no início da lista.
 *
 * Insere o nó passado como argumento @c node ao início da lista. Pode ser usado
 * para empilhar, se quiser usar a lista como uma pilha.
 *
 * @param l     Ponteiro para a lista.
 * @param node  Ponteiro para o nó a ser inserido.
 */
static inline void list_prepend(list_t *l, list_node_t *node)
{
    node->next   = l->head.next;
    l->head.next = node;
}

/**
 * @brief Verifica se a lista está vazia.
 *
 * Verifica se a lista está vazia. Indefinido se a lista não foi iniciada com
 * @c list_init.
 *
 * @param l  Ponteiro para a lista.
 * @return   Se a lista está (@c true) ou não (@c false) vazia.
 */
static inline int list_is_empty(list_t *l)
{
    return l->last == &(l->head);
}

/**
 * @brief Remove o nó seguinte a um dado nó.
 *
 * Remove da lista o nó seguinte ao passado como argumento @c node. Se o último
 * nó for passado como argumento, o sentinela será removido, tornando a lista
 * inconsistente.
 *
 * @param l     Ponteiro para a lista.
 * @param node  Ponteiro para o nó anterior ao nó a ser removido.
 * @return      Retorna o nó removido.
 */
static inline list_node_t *list_remove_next(list_t *l, list_node_t *prev)
{
    list_node_t *node = prev->next;
    if(node == l->last)
        l->last = prev;

    prev->next = node->next;
    return node;
}

/**
 * @brief Remove o primeiro nó da lista.
 *
 * Remove o primeiro nó da lista. Pode ser usado para desenfileirar, no caso de
 * uma fila, ou desempilhar, no caso de uma pilha.
 *
 * @param l  Ponteiro para a lista.
 * @return   Retorna o nó removido.
 */
static inline list_node_t *list_remove_first(list_t *l)
{
    list_node_t *node = l->head.next;
    if(node == l->last)
        l->last = &(l->head);

    l->head.next = node->next;
    return node;
}

/**
 * @brief Acessa os dados referentes ao nó da lista.
 *
 * Acessa os dados do nó passado como argumento @c node, considerando que os
 * dados são do tipo passado em @c type e que dentro desse tipo o nome dado ao
 * membro que contém o nó da lista é o passado em @c member.
 *
 * @param type    O tipo de dados da lista.
 * @param node    Ponteiro para o nó do qual se quer acessar os dados.
 * @param member  Nome do membro de @c type que representa o nó da lista.
 * @return        Resolve em um ponteiro para o dado referente ao nó fornecido.
 */
#define list_entry(type, node, member) \
	((type *)((char *)(node)-(unsigned long)(&((type *)0)->member)))

/**
 * @brief Percorre os nós da lista.
 *
 * Um loop que itera pelos nós da lista.
 *
 * @param l    Ponteiro para a lista.
 * @param pos  Ponteiro que receberá cada nó.
 */
#define list_for_each(l, pos) \
	for (pos = (l)->head.next; \
	     pos != &((l)->head); \
	     pos = pos->next)

/**
 * @brief Percorre os nós da lista, seguro para remoção.
 *
 * Um loop que itera pelos nós da lista. Permite que o nó atual seja removido
 * sem causar problemas na iteração.
 *
 * @param l    Ponteiro para a lista.
 * @param pos  Ponteiro que receberá cada nó.
 * @param n    Ponteiro que receberá o próximo nó.
 */
#define list_for_each_safe(l, pos, n) \
	for (pos = (l)->head.next, n = pos->next; \
	     pos != &((l)->head); \
	     pos = n, n = pos->next)

/**
 * @brief Percorre os dados da lista.
 *
 * Um loop que itera pelos dados da lista.
 *
 * @param l       Ponteiro para a lista.
 * @param pos     Ponteiro que receberá os dados referentes a cada nó.
 * @param member  Nome do membro do tipo de @c pos que representa o nó da lista.
 */
#define list_for_each_entry(l, pos, member) \
	for (pos = list_entry(typeof(*pos), (l)->head.next, member); \
	     &(pos->member) != &((l)->head); \
	     pos = list_entry(typeof(*pos), pos->member.next, member))

/**
 * @brief Percorre os dados da lista, seguro para remoção.
 *
 * Um loop que itera pelos dados da lista. Permite que o nó atual seja removido
 * sem causar problemas na iteração.
 *
 * @param l       Ponteiro para a lista.
 * @param pos     Ponteiro que receberá os dados referentes a cada nó.
 * @param n       Ponteiro que receberá os dados referentes ao próximo nó.
 * @param member  Nome do membro do tipo de @c pos que representa o nó da lista.
 */
#define list_for_each_entry_safe(l, pos, n, member) \
	for (pos = list_entry(typeof(*pos), (l)->head.next, member), \
	     n = list_entry(typeof(*pos), pos->member.next, member); \
	     &(pos->member) != &((l)->head); \
	     pos = n, \
	     n = list_entry(typeof(*pos), pos->member.next, member))

#endif
