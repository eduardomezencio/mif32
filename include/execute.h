/**
 * @file   execute.h
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Declaração de funções para simulação das instruções.
 *
 * Este arquivo declara as funções que simulam a execução de cada instrução.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#ifndef INCLUDE_EXECUTE_H
#define INCLUDE_EXECUTE_H

#include "instruction.h"

int execute_instruction (const instruction_t *instr);

int execute_addi        (const instruction_t *instr);
int execute_addiu       (const instruction_t *instr);
int execute_add         (const instruction_t *instr);
int execute_addu        (const instruction_t *instr);
int execute_andi        (const instruction_t *instr);
int execute_and         (const instruction_t *instr);
int execute_beq         (const instruction_t *instr);
int execute_bltz        (const instruction_t *instr);
int execute_bne         (const instruction_t *instr);
int execute_div         (const instruction_t *instr);
int execute_divu        (const instruction_t *instr);
int execute_jal         (const instruction_t *instr);
int execute_j           (const instruction_t *instr);
int execute_jr          (const instruction_t *instr);
int execute_lb          (const instruction_t *instr);
int execute_lbu         (const instruction_t *instr);
int execute_lui         (const instruction_t *instr);
int execute_lw          (const instruction_t *instr);
int execute_mfhi        (const instruction_t *instr);
int execute_mflo        (const instruction_t *instr);
int execute_mult        (const instruction_t *instr);
int execute_multu       (const instruction_t *instr);
int execute_nor         (const instruction_t *instr);
int execute_ori         (const instruction_t *instr);
int execute_or          (const instruction_t *instr);
int execute_sb          (const instruction_t *instr);
int execute_sll         (const instruction_t *instr);
int execute_sllv        (const instruction_t *instr);
int execute_slti        (const instruction_t *instr);
int execute_slt         (const instruction_t *instr);
int execute_sra         (const instruction_t *instr);
int execute_srav        (const instruction_t *instr);
int execute_srl         (const instruction_t *instr);
int execute_srlv        (const instruction_t *instr);
int execute_sub         (const instruction_t *instr);
int execute_subu        (const instruction_t *instr);
int execute_sw          (const instruction_t *instr);
int execute_syscall     (const instruction_t *instr);
int execute_xori        (const instruction_t *instr);
int execute_xor         (const instruction_t *instr);

#endif

