/**
 * @file   instruction.h
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Declaração do tipo de dados para instruções.
 *
 * Este arquivo declara o tipo de dados que representa uma instrução e funções
 * de conversão entre este tipo de dados e o formato binário.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#ifndef INCLUDE_INSTRUCTION_H
#define INCLUDE_INSTRUCTION_H

#include <stdint.h>

/**
 * @brief Instrução de formato R.
 *
 * Representação de uma instrução de formato R com um inteiro para cada parte
 * da instrução.
 *
 * @see https://en.wikipedia.org/wiki/MIPS_architecture#Instruction_formats
 */
struct r_instruction
{
    uint8_t  rs;
    uint8_t  rt;
    uint8_t  rd;
    uint8_t  shamt;
    uint8_t  funct;
};

/**
 * @brief Instrução de formato I.
 *
 * Representação de uma instrução de formato I com um inteiro para cada parte
 * da instrução.
 *
 * @see https://en.wikipedia.org/wiki/MIPS_architecture#Instruction_formats
 */
struct i_instruction
{
    uint8_t  opcode;
    uint8_t  rs;
    uint8_t  rt;
    uint16_t immediate;
};

/**
 * @brief Instrução de formato J.
 *
 * Representação de uma instrução de formato J com um inteiro para cada parte
 * da instrução.
 *
 * @see https://en.wikipedia.org/wiki/MIPS_architecture#Instruction_formats
 */
struct j_instruction
{
    uint8_t  opcode;
    uint32_t address;
};

/**
 * @brief Instrução genérica.
 *
 * Representação de uma instrução que usa um @c char para definir o formato da
 * instrução e um @c union com um membro para cada formato.
 *
 * @see https://en.wikipedia.org/wiki/MIPS_architecture#Instruction_formats
 */
struct instruction
{
    char format;                /**< Formato da instrução ('R', 'I', 'J'). */
    union
    {
        struct r_instruction r;
        struct i_instruction i;
        struct j_instruction j;
    } data;                     /**< @c union com membros r, i e j que
                                     representam cada formato de instrução. */
};

typedef struct instruction instruction_t;

// Declaração de funções: documentação na definição
uint32_t       instruction_to_bin (const instruction_t *src);
instruction_t *bin_to_instruction (uint32_t src, instruction_t *dest);

#endif

