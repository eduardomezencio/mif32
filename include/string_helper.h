/**
 * @file   string_helper.h
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Declaração de funções e tipos relacionados a strings.
 *
 * Declaração de tipos e funções para uma lista de strings e funções para
 * tratamento de strings.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#ifndef INCLUDE_STRING_HELPER_H
#define INCLUDE_STRING_HELPER_H

#include "list.h"

/**
 * @brief Entrada da lista de strings.
 *
 * Uma string com um @c list_node_t para ser usado em uma lista.
 */
struct string_list_entry
{
    char        *str;  /**< A string. */
    list_node_t  node; /**< Nó da lista. */
};
typedef struct string_list_entry string_list_entry_t;

// Declaração de funções: documentação na definição
char *sep_comment       (char *src, char **comment);
char *sep_label         (char *src, char **label);
char *sep_args          (char *src, char **args);
char *trim_space        (char *src);

void string_list_append (list_t *l, char *str);
void string_list_free   (list_t *l);

#endif

