/**
 * @file   instr_asm.h
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Declaração de funções para a montagem de instruções.
 *
 * Este arquivo declara funções necessárias ao assembler para transformar
 * entradas de texto em instruções, incluindo a tradução de argumentos e labels.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#ifndef INCLUDE_INSTR_ASM_H
#define INCLUDE_INSTR_ASM_H

#include <stdint.h>
#include "instr_dict.h"
#include "instruction.h"
#include "list.h"

#define REGISTER_ERROR  0xE0
#define IMMEDIATE_ERROR 0x80000000
#define LABEL_ERROR     0xFC000000

// Declaração de funções: documentação na definição
int       pseudo_expand      (const dict_entry_t *instr, char *args,list_t *dest);
int       instruction_encode (const dict_entry_t *instr, char *args,
                                    uint32_t ref_addr, instruction_t *dest);
uint8_t   register_to_bin    (const char *reg);
uint32_t  immediate_to_bin   (const char *immed, instr_arg_t argt);
uint32_t  label_to_bin       (const char *label, char format, uint32_t ref_addr);

#endif
