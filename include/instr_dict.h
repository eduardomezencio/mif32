/**
 * @file   instr_dict.h
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Declaração de tipos e funções para o dicionário de instruções.
 *
 * Este arquivo contém as declarações dos tipos e funções usados pelo
 * dicionário de instruções, que é implementado como uma lista de dict_entry.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#ifndef INCLUDE_INSTR_DICT_H
#define INCLUDE_INSTR_DICT_H

#include <stdint.h>
#include "list.h"

#define INSTR_MAX_NAME_SIZE   7
#define INSTR_MAX_ARGS        3
#define INSTR_MAX_ARG_SIZE    5
#define PSEUDO_MAX_ARGS_SIZE  8
#define PSEUDO_MAX_INSTR      2

/**
 * @brief Tipos de argumento para instruções.
 *
 * O enum instr_arg representa os tipos de argumento que uma instrução pode
 * receber.
 */
enum instr_arg
{
    arg_none,           /**< Nenhum. */
    arg_rs,             /**< Registrador s. */
    arg_rt,             /**< Registrador t. */
    arg_rd,             /**< Registrador d. */
    arg_immed,          /**< Valor imediato. */
    arg_immed_offset,   /**< Valor imediato com offset ( C($s) ). */
    arg_label,          /**< Label. */
    arg_shamt           /**< Valor de shift ammount. */
};
typedef enum instr_arg instr_arg_t;

/**
 * @brief Informações sobre entrada de instrução no dicionário.
 *
 * A struct instr_info contém informações que devem estar presentes numa
 * entrada do dicionário para uma instrução normal.
 *
 * @see pseudo_info
 */
struct instr_info
{
    char         format;       /*< Formato da instrução (R, I, J). */
    uint16_t     opcode;       /*< Opcode (ou funct, no caso de instrução R).*/
    instr_arg_t  args[INSTR_MAX_ARGS]; /**< Tipos dos argumentos. */
};

/**
 * @brief Informações sobre entrada de pseudo-instrução no dicionário.
 *
 * A struct pseudo_info contém informações que devem estar presentes numa
 * entrada do dicionário para uma pseudo-instrução.
 *
 * @see pseudo_info
 */
struct pseudo_info
{
    uint8_t  instr_c; /**< Número de instruções que substituem a pseudo-instrução. */
    uint8_t  arg_c;   /**< Número de argumentos. */
    char     arg_types[INSTR_MAX_ARGS+1]; /**<
              * String com cada caracter representando o tipo de um
              * argumento (r:registrador, a:endereço, l:label). */
    char     instr_names[PSEUDO_MAX_INSTR][INSTR_MAX_NAME_SIZE+1]; /**<
              * Nomes das instruções a serem geradas. */
    char     instr_args[PSEUDO_MAX_INSTR][PSEUDO_MAX_ARGS_SIZE+1]; /**<
              * Argumentos das instruções a serem geradas
              * ( a# é substituido pelo argumento número '#' passado;
              *   u# é substituido pela meia palavra superior do argumento '#'
              *   l# é substituido pela meia palabra inferior do argumento '#'). */
};

/**
 * @brief Entrada do dicionário de instruções.
 *
 * A entrada do dicionário contém informações que tanto instruções normais
 * quanto pseudo-instruções precisam e usa um union para armazenar informações
 * específicas ao tipo de cada instrução. Possui um list_node, para poder ser
 * usado em uma lista.
 *
 * @see instr_info
 * @see pseudo_info
 */
struct dict_entry
{
    char         name[INSTR_MAX_NAME_SIZE+1]; /**< Nome da instrução. */
    char         type; /**< Tipo da instrução ('i':instrução, 'p':pseudo). */
    list_node_t  node; /**< Nó da lista. */

    union {
        struct instr_info  i; /**< Informações de instrução normal. */
        struct pseudo_info p; /**< Informaçoes de pseudo-instrução. */
    } info; /**< Informações específicas para cada tipo de instrução. */
};
typedef struct dict_entry dict_entry_t;

// Declaração de funções: documentação na definição
      int           instr_dict_init   ();
      void          instr_dict_free   ();
const dict_entry_t *instr_dict_search (char *name);
      int           get_instr_c       (const dict_entry_t *entry);

#endif

