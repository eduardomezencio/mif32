/**
 * @file   assembler.h
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Declaração de tipos e funções do assembler.
 *
 * Este arquivo contém as declarações dos tipos e funções relacionadas ao
 * assembler.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#ifndef INCLUDE_ASSEMBLER_H
#define INCLUDE_ASSEMBLER_H

#include <stdint.h>
#include "list.h"

#define STACK_DEFAULT_SIZE  0x00000100
#define SECTION_OFFSET_DATA 0x00000000
#define SECTION_OFFSET_TEXT 0x00000000

/**
 * @brief Seções do assembler.
 *
 * Enumeração das seções de código do assembler.
 */
enum assembler_section
{
    SECTION_NONE,   /**< Antes de iniciar a seção data. */
    SECTION_DATA,   /**< A seção data. */
    SECTION_TEXT,   /**< A seção text. */
    SECTION_FINISH  /**< O código acabou. */
};

/**
 * @brief Diretivas do assembler.
 *
 * Enumeração das diretivas suportadas pelo assembler.
 */
enum assembler_directives
{
    DIR_DATA,   /**< Diretiva .data . */
    DIR_TEXT,   /**< Diretiva .text . */
    DIR_BYTE,   /**< Diretiva .byte . */
    DIR_WORD,   /**< Diretiva .word . */
    DIR_ASCIIZ, /**< Diretiva .asciiz . */
    DIR_SPACE   /**< Diretiva .space . */
};

/**
 * @brief Informações do assembler.
 *
 * Contém todas as informações relativas ao estado do assembler.
 */
struct assembler_info
{
    char                   *in_file;    /**< Nome do arquivo de entrada. */
    char                   *out_file;   /**< Nome do arquivo de saída. */
    enum assembler_section  section;    /**< Seção atual do assembler. */
    uint32_t                address;    /**< Endereço da linha atual. */
    uint32_t                stack_size; /**< Tamanho da pilha de execução em words. */
    uint32_t                data_size;  /**< Tamanho da seção data. */
    uint32_t                text_size;  /**< Tamanho da seção text. */
};

/**
 * @brief Entrada da lista de words.
 *
 * Uma word com um @c list_node_t para ser usado em uma lista.
 */
struct word_list_entry
{
    uint32_t     word; /**< A word. */
    list_node_t  node; /**< Nó da lista. */
};

// Declaração de funções: documentação na definição
int  assembler_init        (char *in_file, char *out_file);
void assembler_close       ();
int  assembler_first_pass  ();
int  assembler_second_pass ();

#endif

