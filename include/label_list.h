/**
 * @file   label_list.h
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Declaração da lista de labels.
 *
 * Este arquivo declara os tipos e funções da lista de labels.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#ifndef INCLUDE_LABEL_LIST_H
#define INCLUDE_LABEL_LIST_H

#include <stdint.h>
#include "assembler.h"
#include "list.h"

#define LABEL_NOT_FOUND 0xFFFFFFFF

/**
 * @brief Entrada da lista de labels.
 *
 * Representação de um label na lista de labels. Contém o nome e o endereço do
 * label, juntamente com um @c list_node_t para a lista.
 */
struct label_list_entry
{
    char         name[32]; /**< O nome do label. */
    uint32_t     address;  /**< O endereço do label. */
    list_node_t  node;     /**< Nó da lista. */
};

// Declaração de funções: documentação na definição
void      label_list_init   ();
void      label_list_free   ();
int       label_list_append (const char *name, uint32_t address,
                             enum assembler_section section);
uint32_t  get_label_address (const char *name);
list_t   *get_data_labels   ();
list_t   *get_text_labels   ();

#endif

