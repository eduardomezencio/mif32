/**
 * @file   io.h
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Declaração de funções para entrada e saída.
 *
 * Este arquivo declara funções de entrada e saída para o assembler.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#ifndef INCLUDE_IO_H
#define INCLUDE_IO_H

#include <stdint.h>
#include <stdio.h>
#include "list.h"

#define IO_LINE_BUFFER_SIZE 256

// Declaração de funções: documentação na definição
int   io_init         (const char *in_file, const char *out_file);
void  io_close        ();
int   io_line_number  ();
void  io_set_echo     (int echo);

char *io_read_line    (char *dest);
char *io_read_line_ne (char *dest);

void  io_write_word   (uint32_t word);
void  io_write_line   (const char *str);
void  io_write_labels (const list_t *labels);
void  io_write_string (const char *str);

/**
 * @brief Imprime mensagem de erro.
 *
 * Imprime uma mensagem de erro em @c stderr.
 *
 * @param str  String que descreve o erro.
 */
#define print_error(str) \
    fprintf(stderr, "Erro: " str "\n")

/**
 * @brief Imprime mensagem de erro com argumentos de formatação.
 *
 * Imprime uma mensagem de erro com argumentos de formatação em @c stderr.
 *
 * @param str  String que descreve o erro.
 * @param ...  Argumentos adicionais passados a fprintf.
 */
#define print_error_f(str, ...) \
    fprintf(stderr, "Erro: " str "\n", __VA_ARGS__)

/**
 * @brief Imprime mensagem de erro com número da linha.
 *
 * Imprime uma mensagem de erro, incluindo o número da linha em que o erro
 * ocorreu, em @c stderr.
 *
 * @param str  String que descreve o erro.
 */
#define print_error_ln(str) \
    fprintf(stderr, "Erro: [linha %d] " str "\n", io_line_number())

/**
 * @brief Imprime mensagem de erro com argumentos de formatação e número da linha.
 *
 * Imprime uma mensagem de erro com argumentos de formatação, incluindo o
 * número da linha em que o erro ocorreu, em @c stderr.
 *
 * @param str  String que descreve o erro.
 * @param ...  Argumentos adicionais passados a fprintf.
 */
#define print_error_ln_f(str, ...) \
    fprintf(stderr, "Erro: [linha %d] " str "\n", io_line_number(), __VA_ARGS__)

/**
 * @brief Imprime sintaxe de linha de comando.
 *
 * Imprime a mensagem de sintaxe da linha de comando do executável.
 */
#define print_syntax(cmd) \
    fprintf(stderr, "Sintaxe: %s arquivo_entrada [arquivo_saida]\n", cmd);

#endif

