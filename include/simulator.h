/**
 * @file   simulator.h
 * @author Eduardo Mezêncio
 * @date   2017
 * @brief  Declaração de tipos e funções do simulador.
 *
 * Este arquivos declara tipos e funções do simulador.
 */
/* -----------------------------------------------------------------------------
 * Copyright 2017 Eduardo Mezêncio
 *
 * Este programa é software livre: você pode redistribuí-lo ou modificá-lo
 * sob os termos da Licença GNU General Public License conforme publicada
 * pela Free Software Foundation, ou na versão 3 da Licença, ou
 * (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que será útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * NEGOCIABILIDADE ou ADEQUAÇÃO A UM PROPÓSITO ESPECÍFICO. Veja a
 * GNU General Public License para mais detalhes.
 *
 * Você deve ter recebido uma cópia da GNU General Public License
 * junto com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------
 */

#ifndef INCLUDE_SIMULATOR_H
#define INCLUDE_SIMULATOR_H

#include <stdint.h>
#include "instruction.h"

/**
 * @brief Enumeração de resultados do simulador.
 *
 * Valores que podem ser retornados por funções do simulador, para indicar qual
 * foi o resultado da operação executada.
 */
enum simulator_result
{
    SIMULATOR_OK,    /**< Simulador deve seguir normalmente. */
    SIMULATOR_ERROR, /**< Erro. Simulador deve interromper execução e acusar erro. */
    SIMULATOR_EXIT   /**< Fim do programa. Simulador deve parar normalmente. */
};

/**
 * @brief Enumeração de seções do simulador.
 *
 * As seções do simulador não são usadas durante a execução, mas durante a
 * leitura dos executáveis e em funções de debug, para indicar qual das memórias
 * (dados ou instruções) deve ser impressa.
 */
enum simulator_section
{
    SECTION_DATA, /**< Seção de dados. */
    SECTION_TEXT  /**< Seção de instruções. */
};

/**
 * @brief Informações do simulador.
 *
 * Contém uma representação da máquina sendo simulada (registradores e memórias)
 * e informações necessárias ao simulador. O valor no PC do simulador estará
 * sempre em words, ou seja, um valor N seria equivalente a 4*N em uma máquina
 * MIPS real.
 */
struct simulator_data
{
    uint32_t       pc;                      /**< Program counter (em words). */
    uint32_t       ir;                      /**< Instruction register. */
    uint32_t       registers[32];           /**< Registradores de uso geral. */

    instruction_t  decoded_instruction;     /**< Instrução atual decodificada. */

    uint32_t       data_memory_size;        /**< Tamanho da memória de dados (em words). */
    uint32_t       instruction_memory_size; /**< Tamanho da memória de instruções (em words). */
    uint32_t      *data_memory;             /**< Memória de dados. */
    uint32_t      *instruction_memory;      /**< Memória de instruções. */
};

// Declaração de funções: documentação na definição
int  simulator_init         (const char *filename);
void simulator_close        ();

int  simulator_fetch        ();
int  simulator_decode       ();
int  simulator_execute      ();

int  simulator_set_pc       (uint32_t address);
int  simulator_jump_pc      (uint32_t address);
int  simulator_advance_pc   (int32_t offset_words);
int  simulator_link         ();
int  simulator_read_byte    (uint32_t address, uint8_t  *dest);
int  simulator_store_byte   (uint32_t address, uint8_t   byte);
int  simulator_read_word    (uint32_t address, uint32_t *dest);
int  simulator_store_word   (uint32_t address, uint32_t  word);
int  simulator_get_string   (uint32_t address, char **dest);
int  simulator_write_string (uint32_t address, size_t n, const char *str);
int  simulator_get_reg      (uint8_t reg, uint32_t *dest);
int  simulator_set_reg      (uint8_t reg, uint32_t value);

void simulator_print_registers ();
void simulator_print_memory    (enum simulator_section s);

#endif

