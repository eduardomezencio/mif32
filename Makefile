CC       = gcc
CFLAGS   =
LIBS     =
DEBUG    = -ggdb

IDIR     = include
ODIR     = obj
SDIR     = src


_DEPS    = assembler.h \
           execute.h \
           instr_asm.h \
           instr_dict.h \
           instruction.h \
           io.h \
           label_list.h \
           list.h \
           simulator.h \
           string_helper.h
DEPS     = $(patsubst %,$(IDIR)/%,$(_DEPS))

_ASM_OBJ = assembler.o \
           instr_asm.o \
           instr_dict.o \
           instruction.o \
           io.o \
           label_list.o \
           string_helper.o \
           mif32asm.o
ASM_OBJ  = $(patsubst %,$(ODIR)/%,$(_ASM_OBJ))

_SIM_OBJ = execute.o \
           instruction.o \
           io.o \
           simulator.o \
           mif32sim.o
SIM_OBJ  = $(patsubst %,$(ODIR)/%,$(_SIM_OBJ))


.PHONY: all all-debug debug clean

all: mif32asm mif32sim

all-debug: CFLAGS+=$(DEBUG)
all-debug: all

debug: all-debug

$(ODIR)/%.o: $(SDIR)/%.c $(DEPS)
	$(CC) -I $(IDIR) -c -o $@ $< $(CFLAGS)

mif32asm: $(ASM_OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

mif32sim: $(SIM_OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

mif32asm-debug: CFLAGS+=$(DEBUG)
mif32asm-debug: mif32asm

mif32sim-debug: CFLAGS+=$(DEBUG)
mif32sim-debug: mif32sim

clean:
	rm -f $(ODIR)/*.o *~ mif32asm mif32sim

